## Steps for building Qu|Nex Documentation Website with Sphinx

### Step-by-step Guide for Updating GitLab pages

1. Clone QuNex Documentation repository from GitLab

   ```
   git@gitlab.com:qunex/qunex-documentation.git
   ```
   
   or

   ```
   https://gitlab.com/qunex/qunex-documentation.git
   ```

2. From the root of the repository execute `buildPages` file by running

   ```
   ./buildPages
   ```

   This file should have taken care of everything. If you encountered errors, make sure you have
   all the dependecies working properly (listed below).

**In order for all the wiki pages to work, make sure that all the source files from BitBucket wiki have a single `.md` extension.**

### Dependencies

In order to execute the steps just described, you have to install the following programs/extensions:

* Python 3.5+
* Sphinx (sphinx-build 2.2.0)
* Sphinx Read the Docs Theme
* Sphinx extension: recommonmark
* m2r
* sed (UNIX stream editor)

More detailed information on installation is provided in the **Installation and Configuration Information**.

#### Installation Steps

1. Create a directory `docs`:

   ```bash
   mkdir docs
   ```

2. Navigate to the docs directory by runing:

   ```bash
   cd docs
   ```

3. In the `docs` directory run `sphinx-quickstart`:

   ```bash
   sphinx-quickstart
   ```

   When prompted `> Separate source and build directories (y/n) [n]:`, enter `y`. Project name, Author name and Project release can be changed later in the `conf.py` file. For the Project language press enter (default: `en`). The responses during `sphinx-quickstart` shoul look simalar to the responses shown below.

   ```bash
   > Separate source and build directories (y/n) [n]: y
   > Project name: QuNexDocs
   > Author name(s): ORIA
   > Project release []: 1.0
   > Project language [en]:
   ```

4. Navigate to `docs/source` directory:

   ```bash
   cd docs/source
   ```

   Then clone the wiki by running:

   ```bash
   git clone git@bitbucket.org:oriadev/qunex.git/wiki
   ```

5. Move to the directory containg the files for building Sphinx documentation by running

   ```bash
   cd docs/source/wiki/Sphinx
   ```

6. Exectute z`buildSphinx` file by running:

   ```bash
   ./buildSphinx
   ```

   If you get an error, make sure to change the access permission of the `buildSphinx` file to execute. (`chmod u+x buildSphinx`)

7. Open `docs/build/index.html` in your browser and test if the website is working properly.

8. If everything is OK, push the documentation data

#### Folder Structure

<img src="/Users/aleksijkraljic/Desktop/SphinxFolderStructure.png" alt="SphinxFolderStructure" style="zoom: 50%;" />

#### Example Qu|Nex Documentation Website Generated with Sphinx

<img src="/Users/aleksijkraljic/Desktop/Screenshot 2019-11-20 at 17.25.33.png" alt="Screenshot 2019-11-20 at 17.25.33" style="zoom: 25%;" />

#### Information for Developers

When adjusting the theme or any other documentation parameters, make sure to modify the files located in `docs/source/wiki/Sphinx`. If you modify the file located elsewhere, for instance `docs/source/_templates/layout.html`, it will apply to the website already generated with Sphinx, but it will be overwritten when sphix html files are generated later. After applying and verifying that changes work and are ready to be imlpemented, push the wiki to the remote repository.

### Installation and Configuration Information

#### Sphinx

Follow the installation instruction from Sphinx website [1]

**Example:** Installing Sphinx on OSX using *brew*:

```
brew install sphinx-doc
```

#### Read The Docs Sphinx Theme

1. Install Read The Docs Sphinx Theme by running [3,7]

   ```bash
   pip install sphinx_rtd_theme
   ```

   or

   ```bash
   pip3 install sphinx_rtd_theme
   ```

2. Configure the `conf.py` file by changing the theme to `'sphinx_rtd_theme'` :

   ```python
   # -- Options for HTML output -------------------------------------------------
   
   # The theme to use for HTML and HTML Help pages.  See the documentation for
   # a list of builtin themes.
   #
   html_theme = 'sphinx_rtd_theme'
   ```

#### recommonmark - an extension for using reStructuredText and Markdown as documentation source

In order to automatically generate the documentation written with Markdown as well as reStructured text, recommonmark package should be installed by running:

```
pip install --upgrade recommonmark
```

After installation, the package should be appended to the list of extensions in the `conf.py` file:

```
extensions = ['sphinxcontrib.matlab', 'sphinx.ext.autodoc', 'recommonmark']
```

If you want to parse Markdown files with other than `.md` extensions, you should adjust the `source_suffix` variable in the `conf.py` file:

```
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}
```

In the example above, `.txt` and `.md` files will be parsed as Markdown, whereas `.rst` files will be parsed as reStructuredText.

#### m2r - markdown to reStructuredText converter

M2R converts a markdown file including reStructuredText (rst) markups to a valid rst format. Install it by running:

```
pip install m2r
```

`m2r` command converts markdown file to rst format, by running:

```
m2r your_document.md [your_document2.md ...]
```

Then you will find `your_document.rst` in the same directory.

#### Spelling - spell check extension

*This extension is not required*

Spelling can be performed automaticaly with the `sphinxcontrib-spelling` extension, installed by running:

```
pip install sphinxcontrib-spelling
```

After installation, it should be appeded to the list of extensions in the `conf.py` file:

```
extensions = ['sphinxcontrib.matlab', 'sphinx.ext.autodoc', 'recommonmark', sphinxcontrib.spelling]
```

Optionally, a variable `spelling_word_list_filename='wordlist.txt'` can be added to thee `conf.py` file in order to ignore words listed in the `wordlist.txt` file located in the same directory as the `conf.py` file.

By typing `make spelling` in the cmdline, it will go throug all the documentation files and output spelling errors along filenames and line numbers.

#### Customizing the Read The Docs theme

Basic them customization can be peformed by declaring certain variables in the `conf.py` file. For instance, some adjustments can be made by pasting this following block to `conf.py` and adjusting the variables:

```python
html_theme_options = {
    'canonical_url': '',
    'analytics_id': 'UA-XXXXXXX-1',  #  Provided by Google in your dashboard
    'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    'vcs_pageview_mode': '',
    'style_nav_header_background': 'white',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': True,
    'titles_only': False
}
```

A logo can be added by adding the following line to the `conf.py` file:

```python
html_logo ='wiki/Images/qunexIcon.png' # path to the logo image file
```

The theme can be further customized using additional CSS code. This can be achieved by adding CSS code within an HTML file.

First, create an empty file in the `docs/source/_templates` directory namend `layout.html`. Copy the following code block in the file:

```html
{% extends "!layout.html" %}
  {% block footer %} {{ super() }}

  <style>
    /* Sidebar header (and topbar for mobile) */
    .wy-side-nav-search, .wy-nav-top {
      background: #000000;
    }
    /* Sidebar */
    .wy-nav-side {
      background: #2f4e8f;
    }
    /* Caption text color */
    .caption-text {
      color: #ffffff;
    }
    /* sidebar references color */
    .reference.internal {
      color: #f7f7f7;
    }
    /* sidebar references color on mouse hover */
    .reference.internal:hover {
      color: #2f4e8f;
      background: #ffffff;
    }
  </style>
{% endblock %}
```

CSS code be then be modifyed withing the `<style>` tags. Which classes to customize with CSS can be determined from the generated HTML files for page (for instance `docs/build/html/index.html`)

#### Configuring Sphinx for Auto-documenting MATLAB projects

Sphinx was primarily written to document Python projects, but an extension for documenting MATLAB code was later developed.

1. Install MATLAB Sphinx domain by running `pip install -U sphinxcontrib-matlabdomain` or `pip3 install -U sphinxcontrib-matlabdomain`. Alternatively, you can install MATLAB Sphinx domain with Easy Install or Distutils (see reference [4]).
2. In your Sphinx `conf.py` file add `'sphinxcontrib.matlab'` to the list of extensions. To use auto-documentation features, add `'sphinx.ext.autodoc'` as well.
3. In order for the Sphinx MATLAB domain to autodocument MATLAB source code, set `matlab_src_dir` to the absolute path instead of appending the path to `sys.path`. Currently, only one MATLAB path can be specified, but all subfolders in that tree will be searched. For convenience, the primary domain can be set to `mat`. [4]

The modified section of the `conf.py` file should look something like this:

```python
# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinxcontrib.matlab', 'sphinx.ext.autodoc']

# matlab source code
matlab_src_dir = os.path.abspath('../myProjectSource')

primary_domain = 'mat'
```

Make sure to uncomment the `import os` statement on the top of `conf.py` if using `os.path.path('..')` when assigning an absolute path to a variable:

```python
# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
```

### Editing the Documentation Source Files

The documentation source files should be located in the `docs` directory. If you want to include photos or other media in the generated pages, it is recommended to store these files in the `_static` directory.

#### Editing `index.rst`

The `index.rst` is the homepage file written in ReStructuredText and it should by default be similar to:

```reStructuredText
.. myProject documentation master file, created by
   sphinx-quickstart on Wed Sep 11 13:07:58 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to myProject's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
```

The file can be structured as desired by the user. By default, it contains the `toctree`, which is the Table of Contents Tree that allows multiple pages to be combined into a single cohesive hierarchy. Pages in the `toctree` are added below the `:caption: Contents:` line (if the Table Of Content Tree caption is desired), as such:

```reStructuredText
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   intro
   examples
```

If you encounter a warning during the build process, at the point when the builder is `checking consistency...`, it is usually a problem with the indentation of `toctree` files, or a file, such as `intro.rst` or `examples.rst` can not be found. The caption of the link in the `toctree` is the same as the heading of the file it refers to. For example, if the first heading in the `intro.rst` file is

```reStructuredText
Introduction
============
```

then the link to the `intro.rst` page will be captioned **Introduction**.

If, for example, a file contains other headings with lower hierarchy, the `toctree` will display as many as it was defined with the `:maxdepth:` parameter. For example, the `examples.rst` file has three leves of headings:

```reStructuredText
Examples
========

Example 1
---------
This is how you do it!

Example 2
---------
Using pip
~~~~~~~~~
Run pip...

Using pip3
~~~~~~~~~~
Run pip3...
```

A comparison of the two Table Of Content Trees built with different settings of the `:maxdepth:`  parameter is shown below.

<img src="/Users/aleksijkraljic/Desktop/SphinxQuNex/toctree.jpg" width="70%" />

#### Project's Source Documentation (auto documentation)

Auto documentation of MATLAB source files (classes, function, etc.) is activated by the `'sphinx.ext.autodoc'` extension set in the `conf.py` file, as shown before. In order for the auto documentation to work properly, the inline documentation should be formatted accordingly (Shown in [6]). Here we present an example on how to document a MATLAB class definition file.

Example: given the following MATLAB source in folder `myProjectSource`:

```matlab
classdef MyHandleClass < handle & my.super.Class
    % a handle class
    %
    % :param x: a variable

    %% some comments
    properties
        x % a property
    end
    methods
        function h = MyHandleClass(x)
            h.x = x
        end
        function x = get.x(obj)
        % how is this displayed?
            x = obj.x
        end
    end
    methods (Static)
        function w = my_static_function(z)
        % A static function in :class:`MyHandleClass`.
        %
        % :param z: input z
        % :returns: w

            w = z
        end
    end
end
```

Use the following to document:

```reStructuredText
myProjectSource
===============
This is the test data module.

.. automodule:: myProjectSource

:mod:`myProjectSource` is a really cool module.

My Handle Class
---------------
This is the handle class definition.

.. autoclass:: MyHandleClass
    :show-inheritance:
    :members:
```

Finally, add the document to the `toctree`:

```
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   examples
   myProjectSource
   support
```

Auto-generated documentation should look like this (after building it):

<img src="/Users/aleksijkraljic/Desktop/MBLab/SphinxQuNex/autodoc.png" width="50%" />

### User Support

The most common approach for user support is to create an IRC channel, a mailing list or a forum. The documentation page created with Sphinx should link users to the support page. **freenode** is an example of a popular IRC network to discuss projects. Pages for declaring issues that users encounter are usually provided by the repository hosting services (BitBucket, Github).

### Additional Information

[1] https://www.sphinx-doc.org/en/master/usage/installation.html

[2] https://restructuredtext.readthedocs.io/en/latest/index.html

[3] https://sphinx-rtd-tutorial.readthedocs.io/en/latest/index.html

[4] https://pypi.org/project/sphinxcontrib-matlabdomain/

[5] https://devanginiblog.wordpress.com/2015/11/16/how-to-properly-document-matlab-code/

[6] https://github.com/sphinx-contrib/matlabdomain

[7] https://sphinx-rtd-theme.readthedocs.io/en/stable/#

[8] https://freenode.net/

[9] https://www.sphinx-doc.org/en/master/usage/markdown.html

[10] https://github.com/miyakogi/m2r

[11] https://pypi.org/project/m2r/

[12]https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html





Written by:

Aleksij Kraljič,
Ljubljana, November 26, 2019