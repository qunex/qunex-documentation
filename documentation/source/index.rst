.. QuNexDocs documentation master file, created by 
   sphinx-quickstart on Fri Nov 15 13:15:28 2019. 
   You can adapt this file completely to your liking, but it should at least 
   contain the root `toctree` directive.]
 
.. image:: wiki/Images/QuNex_Logo_pantheonsite.png 
   :align: center
 
-------------- 

.. toctree:: 
   :maxdepth: 1 
   :caption: General overview 
   :hidden:
 
   Installing from source and dependencies <wiki/Overview/Installation.md> 
   QuNex container deployment <wiki/Overview/QuNexContainerUsage.md> 
   QuNex commands and general usage overview <wiki/Overview/GeneralUse.md> 
   File formats overview <wiki/Overview/FileFormats.md> 
   QuNex data hierarchy specification <wiki/Overview/DataHierarchy.md> 
   Logging and log files <wiki/Overview/Logging.md> 
   Visual schematic of QuNex steps <wiki/Overview/VisualSchematic.md> 
   BIDS support within QuNex <wiki/Overview/QuNexBIDS.md> 
   How to report issues with QuNex <wiki/Overview/QuNexIssues.md> 

.. toctree:: 
   :maxdepth: 1 
   :caption: User guides 
   :hidden:
 
   Running commands against a container using qunex_container <wiki/UsageDocs/RunningQunexContainer.md> 
   QuNex 'turnkey' workflow and 'batch' engine <wiki/UsageDocs/Turnkey.md> 
   Running lists of QuNex commands <wiki/UsageDocs/RunningListsOfCommands.md> 
   Running commands over multiple sessions <wiki/UsageDocs/RunningMultipleSessions.md> 
   Parallel Execution and Scheduling of QuNex commands <wiki/UsageDocs/Scheduling.md> 
   De-identifying DICOM files <wiki/UsageDocs/DICOMDeidentification.md> 
   Preparing the QuNex study folder hierarchy <wiki/UsageDocs/PreparingStudy.md> 
   Onboarding new data into QuNex <wiki/UsageDocs/OnboardingNewData.md> 
   Preparing a study-level mapping file for QuNex workflows <wiki/UsageDocs/PreparingMappingFile.md> 
   Preparing study-level batch file and parameters for QuNex workflows <wiki/UsageDocs/GeneratingBatchFiles.md> 
   Preparing and running the HCP pipeline <wiki/UsageDocs/HCPPreprocessing.md> 
   Running quality control (QC) across modalities <wiki/QC) across modalities](UsageDocs/MultiModalQC.md> 
   BOLD task-evoked and resting-state functional connectivity preprocessing <wiki/UsageDocs/BOLDPreprocessing.md> 
   BOLD movement scrubbing <wiki/UsageDocs/MovementScrubbing.md> 
   Running 1st and 2nd level BOLD task activation analyses <wiki/UsageDocs/BOLDTaskActivation.md> 
   Running BOLD functional connectivity analyses <wiki/UsageDocs/BOLDFunctionalConnectivity.md> 
   Group-level statistics and mapping of results <wiki/UsageDocs/GroupLevelStats.md> 
   Identifying empirical ROI and extracting peak information <wiki/UsageDocs/ROIanalyses.md> 
   Diffusion weighted imaging (DWI) 'legacy' data preprocessing <wiki/DWI) 'legacy' data preprocessing](UsageDocs/DWILegacyPreprocessing.md> 
   Running DWI analyses <wiki/UsageDocs/DWIAnalyses.md> 
   Visualizing data via CIFTI templates <wiki/UsageDocs/CIFTIVisualizationTemplate.md> 
   Mapping processed data in and out of QuNex <wiki/UsageDocs/MappingDataInAndOutQuNex.md> 
   Post-mortem macaque tractography <wiki/UsageDocs/PostMortemMacaque.md> 

.. toctree:: 
   :maxdepth: 1 
   :caption: QuNex deployment in XNAT 
   :hidden:
 
   Using the XNAT upload assistant <wiki/XNAT/UsingXNATUploadAssistant.md> 
   QuNex-XNAT integration via container service <wiki/XNAT/QuNex_XNAT.md> 

.. toctree:: 
   :maxdepth: 1 
   :caption: Reference 
   :hidden:
 
   code_docs/python_docs 
   code_docs/matlab_qx_mri_docs 
   code_docs/matlab_qx_utilities_docs 
   code_docs/bash_docs 

.. include:: wiki/Sphinx/HomeMenu.rst
 
