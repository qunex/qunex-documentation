# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

sys.path.append(os.path.abspath('./qunex/python'))
sys.path.append(os.path.abspath('./qunex/python/qx_utilities'))
#sys.path.append(os.path.abspath('./qunex/python/qx_utilities/general'))
#sys.path.append(os.path.abspath('./qunex/python/qx_utilities/hcp'))


# -- Project information -----------------------------------------------------

project = 'QuNexDocs'
copyright = '2021, Qu|Nex, Anticevic Lab (Yale University), Murray Lab (Yale University), Mind and Brain Lab (University of Ljubljana)'
author = 'qunex'

# The full version, including alpha/beta/rc tags
release = '1'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx_rtd_theme',
'sphinx.ext.autodoc',
'recommonmark',
'sphinx.ext.autosummary',
'sphinxcontrib.matlab',
'sphinx.ext.napoleon']

napoleon_google_docstring = False
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = False
napoleon_include_special_with_doc = False
napoleon_use_param = True
napoleon_use_ivar = True
napoleon_use_rtype = True
add_function_parentheses = False
autosummary_generate = True

matlab_src_dir = os.path.abspath('.')

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

html_css_files = [
    'css/inline_code_wrap.css',
]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_theme_options = {
    'logo_only': True,
    'display_version': True
}

html_logo ='wiki/Images/QuNex_Logo_small.png'