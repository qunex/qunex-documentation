bash qx_utilities
=================

dwi_bedpostx_gpu
----------------

 This function runs the FSL bedpostx_gpu processing using a GPU-enabled
 node or via a GPU-enabled queue if using the scheduler option.
 
 It explicitly assumes the Human Connectome Project folder structure for
 preprocessing and completed diffusion processing. DWI data is expected to
 be in the following folder:
 
  <study_folder>/<case>/hcp/<case>/Diffusion
 
 INPUTS
 ======
 
 --sessionsfolder   Path to study folder that contains sessions
 --sessions         Comma separated list of sessions to run
 --fibers           Number of fibres per voxel. [3]
 --weight           ARD weight, more weight means less secondary
                    fibres per voxel [1]
 --burnin           Burnin period. [1000]
 --jumps            Number of jumps. [1250]
 --sample           Sample every. [25]
 --model            Deconvolution model:
                    - 1 ... with sticks,
                    - 2 ... with sticks with a range of diffusivities,
                    - 3 ... with zeppelins. [2]
 --rician           Replace the default Gaussian noise assumption with
                    Rician noise (yes/no). [yes]
 --gradnonlin       Consider gradient nonlinearities (yes/no). By default set
                    automatically. Set to yes if the file grad_dev.nii.gz
                    is present, set to no if it is not.
 --overwrite        Delete prior run for a given session. [no]
 --scheduler        A string for the cluster scheduler (LSF, PBS or SLURM)
                    followed by relevant options, e.g. for SLURM the string
                    would look like this:
                    --scheduler='SLURM,jobname=<name_of_job>,
                    time=<job_duration>,ntasks=<numer_of_tasks>,
                    cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,
                    partition=<queue_to_send_job_to>'
                    Note: You need to specify a GPU-enabled queue or partition
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/dwi_bedpostx_gpu.sh \
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via::
 
  qunex dwi_bedpostx_gpu --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (LSF, PBS or SLURM)
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call::
                    
  --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,"
 ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,
 mem-per-cpu=<memory>,partition=<queue_to_send_job_to>'
   
 ::
 
  qunex dwi_bedpostx_gpu \
  --sessionsfolder='<path_to_study_sessions_folder>' \
  --sessions='<comma_separarated_list_of_cases>' \
  --fibers='3' \
  --burnin='3000' \
  --model='3' \
  --scheduler='<name_of_scheduler_and_options>' \
  --overwrite='yes'



dwi_dtifit
----------

 This function runs the FSL dtifit processing locally or via a scheduler.
 It explicitly assumes the Human Connectome Project folder structure for 
  preprocessing and completed diffusion processing. 
 
 The DWI data is expected to be in the following folder::
 
   <study_folder>/<case>/hcp/<case>/Diffusion
 
 INPUTS
 ======
 
 --sessionsfolder   Path to study folder that contains sessions
 --sessions         Comma separated list of sessions to run
 --overwrite        Delete prior run for a given session (yes / no)
 --species          dtifit currently supports processing of human and macaque
                    data. If processing macaques set this parameter to macaque.
 --mask             Bet binary mask file [T1w/diffusion/nodif_brain_mask].
 --bvecs            b vectors file [T1w/diffusion/bvecs].
 --bvals            b values file [T1w/diffusion/bvals].
 --cni              Input confound regressors [not set by default].
 --sse              Output sum of squared errors [not set by default].
 --wls              Fit the tensor with weighted least squares
                    [not set by default].
 --kurt             Output mean kurtosis map (for multi-shell data)
                    [not set by default].
 --kurtdir          Output parallel/perpendicular kurtosis maps
                    (for multi-shell data) [not set by default].
 --littlebit        Only process small area of brain [not set by default].
 --save_tensor      Save the elements of the tensor [not set by default].
 --zmin             Min z [not set by default].
 --zmax             Max z [not set by default].
 --ymin             Min y [not set by default].
 --ymax             Max y [not set by default].
 --xmin             Min x [not set by default].
 --xmax             Max x [not set by default].
 --gradnonlin       Gradient nonlinearity tensor file [not set by default].
 --scheduler        A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                    followed by relevant options; e.g. for SLURM the string 
                    would look like this: 
 
                    --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<numer_of_tasks>, cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/dwi_dtifit.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
  qunex dwi_dtifit --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
  --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>, mem-per-cpu=<memory>,partition=<queue_to_send_job_to>'      
      
 ::
 
 qunex dwi_dtifit \ 
 --sessionsfolder='<path_to_study_sessions_folder>' \ 
 --sessions='<comma_separarated_list_of_cases>' \ 
 --scheduler='<name_of_scheduler_and_options>' \ 
 --overwrite='yes'


dwi_eddy_qc
-----------

 This function is based on FSL's eddy to perform quality control on 
 diffusion MRI (dMRI) datasets. It explicitly assumes the that eddy has been 
  run and that EDDY QC by Matteo Bastiani, FMRIB has been installed. 
 
 For full documentation of the EDDY QC please examine the README file.
 
 The function assumes that eddy outputs are saved in the following folder::
 
  <folder_with_sessions>/<case>/hcp/<case>/Diffusion/eddy/
 
 INPUTS
 ======
 
 --sessionsfolder    Path to study folder that contains sessions
 --session           Session ID to run EDDY QC on
 --eddybase          This is the basename specified when running EDDY (e.g. 
                     eddy_unwarped_images)
 --eddyidx           EDDY index file
 --eddyparams        EDDY parameters file
 --mask              Binary mask file (most qc measures will be averaged 
                     across voxels labeled in the mask)
 --bvalsfile         bvals input file
 --report            If you want to generate a group report (individual or 
                     group) [individual]
 --overwrite         Delete prior run for a given session
 --eddypath          Specify the relative path of the eddy folder you want to 
                     use for inputs
                     [<study_folder>/<case>/hcp/<case>/Diffusion/eddy/]
 --bvecsfile         If specified, the tool will create a 
                     bvals_no_outliers.txt and a bvecs_no_outliers.txt file 
                     that contain the bvals and bvecs of the non-outlier 
                     volumes, based on the MSR estimates,
 
 If --report='group', then this argument needs to be specified: 
 
 --list              Text file containing a list of qc.json files obtained 
                     from SQUAD
 
 Extra optional inputs if --report='group' 
 
 --groupvar          Text file containing extra grouping variable
 --outputdir         Output directory ['<eddyBase>.qc']
 --update            Applies only if --report='group' - set to <true> to 
                     update existing single session qc reports 
 
 OUTPUTS
 =======
 
 Outputs for individual run: 
  
 - qc.pdf               ... single session QC report 
 - qc.json              ... single session QC and data info
 - vols_no_outliers.txt ... text file that contains the list of the non-outlier 
   volumes (based on eddy residuals)
 
 Outputs for group run: 
  
 - group_qc.pdf ... single session QC report 
 - group_qc.db  ... database  
 
 EXAMPLE USE
 ===========
 
 ::
 
  dwi_eddy_qc.sh --sessionsfolder='<path_to_study_folder_with_session_directories>' \ 
  --session='<session_id>' \ 
  --eddybase='<eddy_base_name>' \ 
  --report='individual'
  --bvalsfile='<bvals_file>' \ 
  --mask='<mask_file>' \ 
  --eddyidx='<eddy_index_file>' \ 
  --eddyparams='<eddy_param_file>' \ 
  --bvecsfile='<bvecs_file>' \ 
  --overwrite='yes' 



dwi_legacy
----------

 This function runs the DWI preprocessing using the FUGUE method for legacy data 
 that are not TOPUP compatible.
 
 It explicitly assumes the the Human Connectome Project folder structure for 
 preprocessing. 
 
 DWI data needs to be in the following folder::
 
  <study_folder>/<case>/hcp/<case>/Diffusion
 
 T1w data needs to be in the following folder::
 
  <study_folder>/<case>/hcp/<case>/T1w
 
 Note: 
 
 - If PreFreeSurfer component of the HCP Pipelines was run the function will 
   make use of the T1w data [Results will be better due to superior brain 
   stripping].
 - If PreFreeSurfer component of the HCP Pipelines was NOT run the function will 
   start from raw T1w data [Results may be less optimal].
 - If you are this function interactively you need to be on a GPU-enabled node 
   or send it to a GPU-enabled queue.
 
 INPUTS
 ======
 
 --sessionsfolder    Path to study data folder
 --sessions          Comma separated list of sessions to run
 --scanner           Name of scanner manufacturer (siemens or ge supported) 
 --echospacing       EPI Echo Spacing for data [in msec]; e.g. 0.69
 --PEdir             Use 1 for Left-Right Phase Encoding, 2 for 
                     Anterior-Posterior
 --unwarpdir         Direction for EPI image unwarping; e.g. x or x- for LR/RL, 
                     y or y- for AP/PA; may been to try out both -/+ combinations
 --usefieldmap       Whether to use the standard field map (yes / no). If set 
                     to <yes> then the parameter --TE becomes mandatory
 --diffdatasuffix    Name of the DWI image; e.g. if the data is called 
                     <SessionID>_DWI_dir91_LR.nii.gz - you would enter 
                     DWI_dir91_LR
 --overwrite         Delete prior run for a given session (yes / no)
 
 FIELDMAP-SPECFIC INPUT
 ----------------------
 
 --TE                This is the echo time difference of the fieldmap sequence 
                     - find this out form the operator - defaults are *usually* 
                     2.46ms on SIEMENS
 
 
 EXAMPLE USE
 ===========
 
 Examples using Siemens FieldMap (needs GPU-enabled node).
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/DWIPreprocPipelineLegacy.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
  qunex dwi_legacy --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
  --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>'      
     
 ::
 
  qunex dwi_legacy \ 
  --sessionsfolder='<folder_with_sessions>' \ 
  --sessions='<comma_separarated_list_of_cases>' \ 
  --function='dwi_legacy' \ 
  --PEdir='1' \ 
  --echospacing='0.69' \ 
  --TE='2.46' \ 
  --unwarpdir='x-' \ 
  --diffdatasuffix='DWI_dir91_LR' \ 
  --usefieldmap='yes' \ 
  --scanner='siemens' \ 
  --overwrite='yes'
 
 Example with flagged parameters for submission to the scheduler using Siemens 
 FieldMap (needs GPU-enabled queue)::
 
  qunex dwi_legacy \ 
  --sessionsfolder='<folder_with_sessions>' \ 
  --sessions='<comma_separarated_list_of_cases>' \ 
  --function='dwi_legacy' \ 
  --PEdir='1' \ 
  --echospacing='0.69' \ 
  --TE='2.46' \ 
  --unwarpdir='x-' \ 
  --diffdatasuffix='DWI_dir91_LR' \ 
  --scheduler='<name_of_scheduler_and_options>' \ 
  --usefieldmap='yes' \ 
  --scanner='siemens' \ 
  --overwrite='yes' \ 
 
 Example with flagged parameters for submission to the scheduler using GE data 
 without FieldMap (needs GPU-enabled queue)::
 
  qunex dwi_legacy \ 
  --sessionsfolder='<folder_with_sessions>' \ 
  --sessions='<comma_separarated_list_of_cases>' \ 
  --diffdatasuffix='DWI_dir91_LR' \ 
  --scheduler='<name_of_scheduler_and_options>' \ 
  --usefieldmap='no' \ 
  --PEdir='1' \ 
  --echospacing='0.69' \ 
  --unwarpdir='x-' \ 
  --scanner='ge' \ 
  --overwrite='yes' \ 


dwi_parcellate
--------------

 This function implements parcellation on the DWI dense connectomes using a 
 whole-brain parcellation (e.g. Glasser parcellation with subcortical labels 
 included).
 
 It explicitly assumes the the Human Connectome Project folder structure for 
 preprocessing. Dense Connectome DWI data needs to be in the following folder::
 
  <folder_with_sessions>/<case>/hcp/<case>/MNINonLinear/Results/Tractography/
 
 INPUTS
 ======
 
 --sessionsfolder    Path to study data folder
 --session           Comma separated list of sessions to run
 --matrixversion     Matrix solution version to run parcellation on; e.g. 1 or 3
 --parcellationfile  Specify the absolute path of the file you want to use for 
                     parcellation 
                     (e.g. /gpfs/project/fas/n3/Studies/Connectome/Parcellations/glasser_parcellation/LR_Colelab_partitions_v1d_islands_withsubcortex.dlabel.nii)
 --outname           Specify the suffix output name of the pconn file
 --lengths           Parcellate lengths matrix (yes/no) [no]
 --waytotal          Use the waytotal normalized version of the DWI dense 
                     connectome. Default: [none]
 
                     - none     ... without waytotal normalization 
                     - standard ... standard waytotal normalized
                     - log      ... log-transformed waytotal normalized
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/dwi_parcellate.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
   qunex dwi_parcellate --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
   --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
 ::
 
  qunex dwi_parcellate --sessionsfolder='<folder_with_sessions>' \ 
  --sessions='<comma_separarated_list_of_cases>' \ 
  --matrixversion='3' \ 
  --parcellationfile='<dlabel_file_for_parcellation>' \ 
  --overwrite='no' \ 
  --outname='LR_Colelab_partitions_v1d_islands_withsubcortex' \ 
 
 Example with flagged parameters for submission to the scheduler::
 
  qunex dwi_parcellate --sessionsfolder='<folder_with_sessions>' \ 
  --sessions='<comma_separarated_list_of_cases>' \ 
  --matrixversion='3' \ 
  --parcellationfile='<dlabel_file_for_parcellation>' \ 
  --overwrite='no' \ 
  --outname='LR_Colelab_partitions_v1d_islands_withsubcortex' \ 
  --scheduler='<name_of_scheduler_and_options>' \ 


dwi_probtrackx_dense_gpu
------------------------

 This function runs the probtrackxgpu dense whole-brain connectome generation by 
 calling ${ScriptsFolder}/run_matrix1.sh or ${ScriptsFolder}/run_matrix3.sh.
 Note that this function needs to send work to a GPU-enabled queue or you need 
 to run it locally from a GPU-equiped machine.
 
 It explicitly assumes the Human Connectome Project folder structure and 
 completed dwi_bedpostx_gpu and dwi_pre_tractography functions processing:
      
  - HCP Pipelines
  - FSL 5.0.9 or greater
 
 Processed DWI data needs to be here::
 
  <study_folder>/<session>/hcp/<session>/T1w/Diffusion
 
 BedpostX output data needs to be here::
 
  <study_folder>/<session>/hcp/<session>/T1w/Diffusion.bedpostX
 
 T1w images need to be in MNINonLinear space here::
 
  <study_folder>/<session>/hcp/<session>/MNINonLinear
 
 INPUTS
 ======
 
 --sessionsfolder          Path to study folder that contains sessions
 --sessions                Comma separated list of sessions to run
 --overwrite               Delete a prior run for a given session (yes / no) [Note: 
                           this will delete only the Matrix run specified by the 
                           -omatrix flag]
 --omatrix1                Specify if you wish to run matrix 1 model [yes or omit flag]
 --omatrix3                Specify if you wish to run matrix 3 model [yes or omit flag]
 --nsamplesmatrix1         Number of samples [10000]
 --nsamplesmatrix3         Number of samples [3000]
 --nsamplesmatrix1         Number of samples [10000]
 --nsamplesmatrix3         Number of samples [3000]
 --distancecorrection      Use distance correction [no]
 --storestreamlineslength  Store average length of the streamlines [no]
 --scriptsfolder           Location of the probtrackX GPU scripts
 
 Generic parameters set by default (will be parameterized in the future)::
 
  --loopcheck --forcedir --fibthresh=0.01 -c 0.2 --sampvox=2 --randfib=1 -S 2000 \ 
  --steplength=0.5
 
 OUTPUTS
 =======
 
 Dense Connectome CIFTI Results in MNI space for Matrix1 will be here::
 
    <study_folder>/<session>/hcp/<session>/MNINonLinear/Results/Conn1.dconn.nii.gz
 
 Dense Connectome CIFTI Results in MNI space for Matrix3 will be here::
 
    <study_folder>/<session>/hcp/<session>/MNINonLinear/Results/Conn3.dconn.nii.gz
 
 USE
 ===
 
 The function calls either of these based on the --omatrix1 and --omatrix3 flags:: 
 
  $HCPPIPEDIR_dMRITractFull/tractography_gpu_scripts/run_matrix1.sh
  $HCPPIPEDIR_dMRITractFull/tractography_gpu_scripts/run_matrix3.sh
 
 Both functions are cluster-aware and send the jobs to the GPU-enabled queue. 
 They do not work interactively.
 
 NOTES
 =====
 
 Note on waytotal normalization and log transformation of streamline counts:
 
 waytotal normalization is computed automatically as part of the run prior to 
 any inter-session or group comparisons to account for individual differences in 
 geometry and brain size. The function divides the dense connectome by the 
 waytotal value, turning absolute streamline counts into relative proportions of 
 the total streamline count in each session. 
 
 Next, a log transformation is computed on the waytotal normalized data, which 
 will yield stronger connectivity values for longe-range projections. 
 Log-transformation accounts for algorithmic distance bias in tract generation 
 (path probabilities drop with distance as uncertainty is accumulated).
 
 See Donahue et al. (2016) The Journal of Neuroscience, 36(25):6758–6770. 
 DOI: https://doi.org/10.1523/JNEUROSCI.0493-16.2016
 
 The outputs for these files will be in::
 
  /<path_to_study_sessions_folder>/<session>/hcp/<session>/MNINonLinear/Results/Tractography/<MatrixName>_waytotnorm.dconn.nii
  /<path_to_study_sessions_folder>/<session>/hcp/<session>/MNINonLinear/Results/Tractography/<MatrixName>_waytotnorm_log.dconn.nii
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/dwi_probtrackx_dense_gpu.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
  qunex dwi_probtrackx_dense_gpu --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
  --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>'      
 
 ::
 
  qunex dwi_probtrackx_dense_gpu --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separarated_list_of_cases>' \ 
  --scheduler='<name_of_scheduler_and_options>' \ 
  --omatrix1='yes' \ 
  --nsamplesmatrix1='10000' \ 
  --overwrite='no'



dwi_seed_tractography_dense
---------------------------

 This function implements reduction on the DWI dense connectomes using a given 
 'seed' structure (e.g. thalamus).
 
 It explicitly assumes the the Human Connectome Project folder structure for 
 preprocessing. Dense Connectome DWI data needs to be in the following folder::
 
  <folder_with_sessions>/<case>/hcp/<case>/MNINonLinear/Results/Tractography/
 
 It produces the following outputs::
 
 - Dense connectivity seed tractography file:
   <folder_with_sessions>/<case>/hcp/<case>/MNINonLinear/Results/Tractography/<session>_Conn<matrixversion>_<outname>.dconn.nii
 - Dense scalar seed tractography file:
   <folder_with_sessions>/<case>/hcp/<case>/MNINonLinear/Results/Tractography/<session>_Conn<matrixversion>_<outname>_Avg.dscalar.nii
 
 INPUTS
 ======
 
 --sessionsfolder    Path to study folder that contains sessions
 --sessions          Comma separated list of sessions to run
 --matrixversion     Matrix solution verion to run parcellation on; e.g. 1 or 3
 --seedfile          Specify the absolute path of the seed file you want to use 
                     as a seed for dconn reduction 
                     (e.g. <study_folder>/<case>/hcp/<case>/MNINonLinear/Results/Tractography/CIFTI_STRUCTURE_THALAMUS_RIGHT.nii.gz)
 
                     Note: If you specify --seedfile='gbc' then the function 
                     computes an average across all streamlines from every 
                     greyordinate to all other greyordinates.
 --outname           Specify the suffix output name of the dscalar file
 --overwrite         Delete prior run for a given session (yes / no)
 --waytotal          Use the waytotal normalized version of the DWI dense 
                     connectome. Default: [none]
 
                     - none     ... without waytotal normalization 
                     - standard ... standard waytotal normalized
                     - log      ... log-transformed waytotal normalized
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/dwi_seed_tractography_dense.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
   qunex dwi_seed_tractography_dense --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
   --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
 ::
 
  qunex dwi_seed_tractography_dense --sessionsfolder='<folder_with_sessions>' \ 
  --session='<case_id>' \ 
  --matrixversion='3' \ 
  --seedfile='<folder_with_sessions>/<case>/hcp/<case>/MNINonLinear/Results/Tractography/CIFTI_STRUCTURE_THALAMUS_RIGHT.nii.gz' \ 
  --overwrite='no' \ 
  --outname='THALAMUS'



data_sync
---------

 This function runs rsync across the entire folder structure based on user 
 specifications. It is used for syncing and backing up folders and data onto 
 local or remote servers. 
 
 INPUTS
 ======
 
 --syncfolders        Set path for folders that contains studies for syncing
 --syncserver         Set sync server <UserName@some.server.address> or 'local' 
                      to sync locally
 --syncdestination    Set sync destination path
 --synclogfolder      Set log folder
 --sessions           Comma separated list of sessions for sync. (optional) 
                      If set, then '--syncfolders' path has to contain sessions' 
                      folders.



extract_roi
-----------

 This function calls img_roi_extract.m and extracts data from an input file for 
 every ROI in a given template file. The function needs a matching file type for 
 the ROI input and the data input (i.e. both NIFTI or CIFTI). It assumes that 
 the template ROI file indicates each ROI in a single volume via unique scalar 
 values.
 
 INPUTS
 ======
 
 --roifile      Path ROI file (either a NIFTI or a CIFTI with distinct scalar 
                values per ROI)
 --inputfile    Path to input file to be read that is of the same type as 
                --roifile (i.e. CIFTI or NIFTI)
 --outpath      New or existing directory to save outputs in
 --outname      Output file base-name (to be appended with 'ROIn')
 
 OUTPUT
 ======
 
 <output_name>.csv
    matrix with one ROI per row and one column per frame in singleinputfile 
 
 EXAMPLE USE
 ===========
 
 ::
 
  qunex roi_extract \ 
  --roifile='<path_to_roifile>' 
  --inputfile='<path_to_inputfile>' 
  --outdir='<path_to_outdir>' 
  --outname='<output_name>'



fc_compute_wrapper
------------------

 This function implements Global Brain Connectivity (GBC) or seed-based 
 functional connectivity (FC) on the dense or parcellated (e.g. Glasser 
 parcellation).
 
 For more detailed documentation run <help fc_compute_gbc3>, 
 <help nimage.img_compute_gbc> or <help fc_compute_seedmaps_multiple> inside MATLAB.
 
 INPUTS
 ======
 
 --calculation    Run <seed>, <gbc> or <dense> calculation for functional
                  connectivity.
 --runtype        Run calculation on a <list> (requires a list input), on 
                  <individual> sessions (requires manual specification) or a 
                  <group> of individual sessions (equivalent to a list, but 
                  with manual specification)
 --targetf        Specify the absolute path for output folder. If using 
                  --runtype='individual' and left empty the output will 
                  default to --inputpath location for each session
 --overwrite      Delete prior run for a given session [no].
 --covariance     Whether to compute covariances instead of correlations 
                  (true / false). Default is [false]
 
 INPUTS FOR A GROUP SEED/GBC RUN
 ===============================
 
 --flist          Specify *.list file of session information. If specified then 
                  --sessionsfolder, --inputfile, --session and --outname are
                  omitted
 
 INPUTS FOR AN INDIVIDUAL SESSION SEED/GBC RUN
 =============================================
 
 --sessionsfolder   Path to study sessions folder
 --sessions         Comma separated list of sessions to run
 --inputfiles       Specify the comma separated file names you want to use 
                    (e.g. /bold1_Atlas_MSMAll.dtseries.nii,bold2_Atlas_MSMAll.dtseries.nii)
 --inputpath        Specify path of the file you want to use relative to the 
                    master study folder and session directory 
                    (e.g. /images/functional/)
 --outname          Specify the suffix name of the output file name  
 
 INPUTS FOR GBC
 ==============
 
 --target           Array of ROI codes that define target ROI
                    [default: FreeSurfer cortex codes]
 --rsmooth          Radius for smoothing (no smoothing if empty). []
 --rdilate          Radius for dilating mask (no dilation if empty). []
 --gbc-command      Specify the the type of gbc to run. This is a string 
                    describing GBC to compute. 
                    E.g. 'mFz:0.1|mFz:0.2|aFz:0.1|aFz:0.2|pFz:0.1|pFz:0.2' 
 
                    mFz:t
                        computes mean Fz value across all voxels (over 
                        threshold t)
                    aFz:t
                        computes mean absolute Fz value across all voxels 
                        (over threshold t) 
                    pFz:t
                        computes mean positive Fz value across all voxels 
                        (over threshold t) 
                    nFz:t
                        computes mean positive Fz value across all voxels 
                        (below threshold t) 
                    aD:t
                        computes proportion of voxels with absolute r over t 
                    pD:t
                        computes proportion of voxels with positive r over t 
                    nD:t
                        computes proportion of voxels with negative r below t 
                    mFzp:n
                        computes mean Fz value across n proportional ranges 
                    aFzp:n
                        computes mean absolute Fz value across n proportional 
                        ranges 
                    mFzs:n
                        computes mean Fz value across n strength ranges 
                    pFzs:n
                        computes mean Fz value across n strength ranges for 
                        positive correlations 
                    nFzs:n
                        computes mean Fz value across n strength ranges for 
                        negative correlations 
                    mDs:n
                        computes proportion of voxels within n strength ranges 
                        of r 
                    aDs:n
                        computes proportion of våoxels within n strength 
                        ranges of absolute r 
                    pDs:n
                        computes proportion of voxels within n strength ranges 
                        of positive r 
                    nDs:n
                        computes proportion of voxels within n strength ranges 
                        of negative r   
 
 --verbose          Report what is going on. Default is [false]
 --time             Whether to print timing information. [false]
 --vstep            How many voxels to process in a single step. [1200]
 
 INPUTS FOR SEED FC
 ==================
 
 --roinfo           An ROI file for the seed connectivity 
 --method           Method for extracting timeseries - 'mean' or 'pca' ['mean'] 
 --options          A string defining which session files to save. Default 
                    assumes all [''] 
 
                    - r ... save map of correlations 
                    - f ... save map of Fisher z values 
                    - cv ... save map of covariances 
                    - z ... save map of Z scores 
 
 INPUTS FOR SEED OR GBC
 ======================
 
 --extractdata      Specify if you want to save out the matrix as a CSV file 
                    (only available if the file is a ptseries) 
 --ignore           The column in *_scrub.txt file that matches bold file to 
                    be used for ignore mask. All if empty. Default is [] 
 --mask             An array mask defining which frames to use (1) and which 
                    not (0). All if empty. If single value is specified then 
                    this number of frames is skipped.
 
 INPUTS FOR DENSE FC RUN
 =======================
 
 --mem-limit        Restrict memory. Memory limit expressed in gigabytes. [4]
 
 EXAMPLE USE
 ===========
 
 Run directly via:: 
 
   ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/fc_compute_wrapper.sh \ 
       --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
   qunex fc_compute_wrapper --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
   --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
 
 ::
 
   qunex fc_compute_wrapper \ 
   --sessionsfolder='<folder_with_sessions>' \ 
   --calculation='seed' \ 
   --runtype='individual' \ 
   --sessions='<comma_separarated_list_of_cases>' \ 
   --inputfiles='<files_to_compute_connectivity_on>' \ 
   --inputpath='/images/functional' \ 
   --extractdata='yes' \ 
   --ignore='udvarsme' \ 
   --roinfo='ROI_Names_File.names' \ 
   --options='' \ 
   --method='' \ 
   --targetf='<path_for_output_file>' \ 
   --mask='5' \ 
   --covariance='false' 
 
   qunex fc_compute_wrapper \ 
   --sessionsfolder='<folder_with_sessions>' \ 
   --runtype='list' \ 
   --flist='sessions.list' \ 
   --extractdata='yes' \ 
   --outname='<name_of_output_file>' \ 
   --ignore='udvarsme' \ 
   --roinfo='ROI_Names_File.names' \ 
   --options='' \ 
   --method='' \ 
   --targetf='<path_for_output_file>' \ 
   --mask='5' 
   --covariance='false' 
 
   qunex fc_compute_wrapper \ 
   --sessionsfolder='<folder_with_sessions>' \ 
   --calculation='gbc' \ 
   --runtype='individual' \ 
   --sessions='<comma_separarated_list_of_cases>' \ 
   --inputfiles='bold1_Atlas_MSMAll.dtseries.nii' \ 
   --inputpath='/images/functional' \ 
   --extractdata='yes' \ 
   --outname='<name_of_output_file>' \ 
   --ignore='udvarsme' \ 
   --gbc-command='mFz:' \ 
   --targetf='<path_for_output_file>' \ 
   --mask='5' \ 
   --target='' \ 
   --rsmooth='0' \ 
   --rdilate='0' \ 
   --verbose='true' \ 
   --time='true' \ 
   --vstep='10000'
   --covariance='false' 
 
   qunex fc_compute_wrapper \ 
   --sessionsfolder='<folder_with_sessions>' \ 
   --calculation='gbc' \ 
   --runtype='list' \ 
   --flist='sessions.list' \ 
   --extractdata='yes' \ 
   --outname='<name_of_output_file>' \ 
   --ignore='udvarsme' \ 
   --gbc-command='mFz:' \ 
   --targetf='<path_for_output_file>' \ 
   --mask='5' \ 
   --target='' \ 
   --rsmooth='0' \ 
   --rdilate='0' \ 
   --verbose='true' \ 
   --time='true' \ 
   --vstep='10000'
   --covariance='false' 



parcellate_anat
---------------

 This function implements parcellation on the dense cortical thickness OR myelin 
 files using a whole-brain parcellation (e.g. Glasser parcellation with 
 subcortical labels included).
 
 INPUTS
 ======
 
  --sessionsfolder     Path to study data folder
  --session            Comma separated list of sessions to run
  --inputdatatype      Specify the type of dense data for the input file (e.g. 
                       MyelinMap_BC or corrThickness)
  --parcellationfile   Specify the absolute path of the *.dlabel file you want 
                       to use for parcellation
  --outname            Specify the suffix output name of the pconn file
  --overwrite          Delete prior run for a given session (yes / no)
  --extractdata        Specify if you want to save out the matrix as a CSV file
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/parcellate_anat.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via::
 
  qunex parcellate_anat \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call:
 
   --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                     followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
   --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
  qunex parcellate_anat --sessionsfolder='<folder_with_sessions>' \ 
  --session='<case_id>' \ 
  --inputdatatype='MyelinMap_BC' \ 
  --parcellationfile='<dlabel_file_for_parcellation>' \ 
  --overwrite='no' \ 
  --extractdata='yes' \ 
  --outname='<name_of_output_pconn_file>' 



parcellate_bold
---------------

 This function implements parcellation on the BOLD dense files using a 
 whole-brain parcellation (e.g. Glasser parcellation with subcortical labels
 included).
 
 INPUTS
 ======
 
 --sessionsfolder     Path to study folder that contains sessions
 --sessions           Comma separated list of sessions to run
 --inputfile          Specify the name of the file you want to use for 
                      parcellation (e.g. bold1_Atlas_MSMAll_hp2000_clean)
 --inputpath          Specify path of the file you want to use for parcellation 
                      relative to the master study folder and session directory 
                      (e.g. /images/functional/)
 --inputdatatype      Specify the type of data for the input file (e.g. dscalar 
                      or dtseries)
 --parcellationfile   Specify the absolute path of the file you want to use for 
                      parcellation 
                      (e.g. /gpfs/project/fas/n3/Studies/Connectome/Parcellations/glasser_parcellation/LR_Colelab_partitions_v1d_islands_withsubcortex.dlabel.nii)
 --singleinputfile    Parcellate only a single file in any location. Individual 
                      flags are not needed (--session, --sessionsfolder, --inputfile).
 --overwrite          Delete prior run (yes / no)
 --computepconn       Specify if a parcellated connectivity file should be 
                      computed (pconn). This is done using covariance and 
                      correlation (yes/no) [no]
 --outname            Specify the suffix output name of the pconn file
 --outpath            Specify the output path name of the pconn file relative 
                      to the master study folder (e.g. /images/functional/)
 --useweights         If computing a parcellated connectivity file you can 
                      specify which frames to omit (e.g. yes or no) [no] 
 --weightsfile        Specify the location of the weights file relative to the 
                      master study folder 
                      (e.g. /images/functional/movement/bold1.use)
 --extractdata        Specify if you want to save out the matrix as a CSV file
 
 EXAMPLES
 ========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/parcellate_bold.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
   qunex parcellate_bold --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
   --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
 ::
 
  parcellate_bold.sh --sessionsfolder='<folder_with_sessions>' \ 
  --session='<session_id>' \ 
  --inputfile='<name_of_input_file' \ 
  --inputpath='<path_for_input_file>' \ 
  --inputdatatype='<type_of_dense_data_for_input_file>' \ 
  --parcellationfile='<dlabel_file_for_parcellation>' \ 
  --overwrite='no' \ 
  --extractdata='yes' \ 
  --outname='<name_of_output_pconn_file>' \ 
  --outpath='<path_for_output_file>'



qunex_acceptance_test
---------------------

 This function implements QuNex acceptance testing per pipeline unit.
 
 INPUTS
 ======
 
 Local system variables if using QuNex hierarchy:
 
 --acceptancetest   Specify if you wish to run a final acceptance test after 
                    each unit of processing.
                    Supported: ${SupportedAcceptanceTestSteps}
 --studyfolder      Path to study data folder
 --sessionsfolder   Path to study data folder where the sessions folders reside
 --subjects         Comma separated list of subjects to run that are 
                    study-specific and correspond to XNAT database subject IDs
 --sessionlabels    Label for session within project. Note: may be general 
                    across multiple subjects (e.g. rest) or for longitudinal 
                    runs.
 --runtype          Default is [], which executes a local file system run, but 
                    requires --studyfolder to set
 
 INPUTS RELATED TO XNAT
 ----------------------
 
 Note: To invoke this function you need a credential file in your home folder or 
 provide one using --xnatcredentials parameter or --xnatuser and --xnatpass 
 parameters:  
 
 --xnatcredentialfile    Specify XNAT credential file name. [${HOME}/.xnat]
                         This file stores the username and password for the 
                         XNAT site. Permissions of this file need to be set to 
                         400. If this file does not exist the script will 
                         prompt you to generate one using default name. If user 
                         provided a file name but it is not found, this name 
                         will be used to generate new credentials. User needs 
                         to provide this specific credential file for next run, 
                         as script by default expects ${HOME}/.xnat
 --xnatprojectid         Specify the XNAT site project id. This is the Project  
                         ID in XNAT and not the Project Title. This project 
                         should be created on the XNAT Site prior to upload. If 
                         it is not found on the XNAT Site or not provided then 
                         the data will land into the prearchive and be left 
                         unassigned to a project.
                         Please check upon completion and specify assignment 
                         manually.
 --xnathost              Specify the XNAT site hostname URL to push data to.
 --xnatuser              Specify XNAT username required if credential file is 
                         not found
 --xnatpass              Specify XNAT password required if credential file is 
                         not found
 --bidsformat            Specify if XNAT data is in BIDS format (yes/no). [no]
                         If --bidsformat='yes' then the subject naming follows 
                         <SubjectLabel_SessionLabel> convention
 --xnatsubjectid         ID for subject across the entire XNAT database. 
                         Required or --xnatsubjectlabels needs to be set.
 --xnatsubjectlabels     Label for subject within a project for the XNAT 
                         database. Default assumes it matches --subjects.
                         If your XNAT database subject label is distinct from 
                         your local server subject id then please supply this 
                         flag.
                         Use if your XNAT database has a different set of 
                         subject ids.
 --xnatsessionlabels     Label for session within XNAT project. Note: may be 
                         general across multiple subjects (e.g. rest). Required.
 --xnataccsessionid      ID for subject-specific session within the XNAT 
                         project. Derived from XNAT but can be set manually.
 --resetcredentials      Specify <yes> if you wish to reset your XNAT site user 
                         and password. Default is [no]
 --xnatgetqc             Specify if you wish to download QC PNG images and/or 
                         scene files for a given acceptance unit where QC is 
                         available. Default is [no]. Options: 
 
                         - 'image' ... download only the image files 
                         - 'scene' ... download only the scene files 
                         - 'all'   ... download both png images and scene files
 
 --xnatarchivecommit     Specify if you wish to commit the results of 
                         acceptance testing back to the XNAT archive. Default 
                         is [no]. Options: 
 
                         -'session' ... commit to subject session only 
                         -'project' ... commit group results to project only 
                         -'all'     ... commit both subject and group results
 
 BOLD PROCESSING ACCEPTANCE TEST INPUTS
 --------------------------------------
 
 --bolddata              Specify BOLD data numbers separated by comma or pipe. 
                         E.g. --bolddata='1,2,3,4,5'. This flag is 
                         interchangeable with --bolds or --boldruns to allow 
                         more redundancy in specification
                         Note: If unspecified empty the QC script will by 
                         default look into 
                         /<path_to_study_sessions_folder>/<session_id>/session_hcp.txt
                         and identify all BOLDs to process
 --boldimages            Specify a list of required BOLD images separated by 
                         comma or pipe. Where the number of the bold image 
                         would be, indicate by '{N}', e.g:
                         --boldimages='bold{N}_Atlas.dtseries.nii|seed_bold{N}_Atlas_s_hpss_res-VWMWB_lpss_LR-Thal.dtseriesnii' 
                         When running the test, '{N}' will be replaced by the 
                         bold numbers given in --bolddata 
 
 EXAMPLE USE
 ===========
 
 ::
 
  qunex_acceptance_test.sh --studyfolder='<absolute_path_to_study_folder>' \ 
  --subjects='<subject_IDs_on_local_server>' \ 
  --xnatprojectid='<name_of_xnat_project_id>' \ 
  --xnathost='<XNAT_site_URL>' 



run_qc
------

 This function runs the QC preprocessing for a specified modality / processing 
 step.
 
 Currently Supported: ${SupportedQC}
 
 This function is compatible with both legacy data [without T2w scans] and 
 HCP-compliant data [with T2w scans and DWI].
 
 With the exception of rawNII, the function generates 3 types of outputs, which 
 are stored within the Study in <path_to_folder_with_sessions>/QC 
 
 - .scene files that contain all relevant data loadable into Connectome Workbench
 - .png images that contain the output of the referenced scene file.
 - .zip file that contains all relevant files to download and re-generate the scene in Connectome Workbench.
 
 Note: For BOLD data there is also an SNR txt output if specified.
 
 Note: For raw NIFTI QC outputs are generated in: 
 <sessions_folder>/<case>/nii/slicesdir
 
 INPUTS
 ======
 
 --sessionsfolder       Path to study folder that contains sessions
 --sessions             Comma separated list of sessions to run
 --modality             Specify the modality to perform QC on. 
                        Supported: rawNII, T1w, T2w, myelin, BOLD, DWI, general, 
                        eddyQC
 
                        Note: If selecting 'rawNII' this function performs QC  
                        for raw NIFTI images in <sessions_folder>/<case>/nii 
                        It requires NIFTI images in <sessions_folder>/<case>/nii/ 
                        after either BIDS import of DICOM organization. 
 
                        Session-specific output: 
                        <sessions_folder>/<case>/nii/slicesdir 
 
                        Uses FSL's 'slicesdir' script to generate PNGs and an 
                        HTML file in the above directory. 
 
                        Note: If using 'general' modality, then visualization is 
                        $TOOLS/$QUNEXREPO/qx_library/data/scenes/qc/template_general_qc.wb.scene
 
                        This will work on any input file within the 
                        session-specific data hierarchy.
 
 --datapath             Required ==> Specify path for input path relative to the 
                        <sessions_folder> if scene is 'general'.
 --datafile             Required ==> Specify input data file name if scene is 
                        'general'.
 --sessionsbatchfile    Absolute path to local batch file with pre-configured 
                        processing parameters.  
 
                        Note: It can be used in combination with --sessions to 
                        select only specific cases to work on from the batch 
                        file. 
 
                        Note: If --sessions is omitted in favor of 
                        --sessionsbatchfile OR if batch file is provided as 
                        input for --sessions flag, then all cases from the batch 
                        file are processed.
 --overwrite            Delete prior QC run: yes/no [no]
 --hcp_suffix           Allows user to specify session id suffix if running HCP 
                        preprocessing variants [].
                        E.g. ~/hcp/sub001 & ~/hcp/sub001-run2 ==> Here 'run2' 
                        would be specified as --hcp_suffix='-run2' 
 --scenetemplatefolder  Specify the absolute path name of the template folder 
                        [default: ${TOOLS}/${QUNEXREPO}/qx_library/data/scenes/qc]
 
                        Note: relevant scene template data has to be in the same 
                        folder as the template scenes.
 --outpath              Specify the absolute path name of the QC folder you wish 
                        the individual images and scenes saved to.
                        If --outpath is unspecified then files are saved to: 
                        /<path_to_study_sessions_folder>/QC/<input_modality_for_qc>
 --scenezip             Yes or no. Generates a ZIP file with the scene and all 
                        relevant files for Connectome Workbench visualization 
                        [yes]
 
                        Note: If scene zip set to yes, then relevant scene files 
                        will be zipped with an updated relative base folder. 
                        All paths will be relative to this base --> 
                        <path_to_study_sessions_folder>/<session_id>/hcp/<session_id>
 
                        The scene zip file will be saved to: 
                        /<path_for_output_file>/<session_id>.<input_modality_for_qc>.QC.wb.zip
 --userscenefile        User-specified scene file name. --modality info is still 
                        required to ensure correct run. Relevant data needs to 
                        be provided. Default []
 --userscenepath        Path for user-specified scene and relevant data in the 
                        same location. --modality info is still required to 
                        ensure correct run. Default []
 --timestamp            Allows user to specify unique time stamp or to parse a 
                        time stamp from QuNex bash wrapper
 --suffix               Allows user to specify unique suffix or to parse a time 
                        stamp from QuNex bash wrapper.
                        Default is [<session_id>_<timestamp>]
 --batchfile            Batch file with pre-configured BOLD tags for quick 
                        filtering of BOLD runs. 
 
                        Note: This file needs to be created *manually* prior to 
                        starting function and absolute path provided in the 
                        function.
 
 DWI INPUTS
 ----------
 
 --dwipath           Specify the input path for the DWI data (may differ across 
                     studies; e.g. Diffusion or Diffusion or 
                     Diffusion_DWI_dir74_AP_b1000b2500)
 --dwidata           Specify the file name for DWI data (may differ across 
                     studies; e.g. data or DWI_dir74_AP_b1000b2500_data)
 --dtifitqc          Specify if dtifit visual QC should be completed (e.g. yes 
                     or no)
 --bedpostxqc        Specify if BedpostX visual QC should be completed (e.g. yes 
                     or no)
 --eddyqcstats       Specify if EDDY QC stats should be linked into QC folder 
                     and motion report generated (e.g. yes or no)
 --dwilegacy         Specify if DWI data was processed via legacy pipelines 
                     (e.g. yes or no)
 
 BOLD INPUTS
 -----------
 
 --boldprefix        Specify the prefix file name for BOLD dtseries data (may 
                     differ across studies depending on processing; e.g. BOLD or 
                     TASK or REST).
                     Note: If unspecified then QC script will assume that folder 
                     names containing processed BOLDs are named numerically only 
                     (e.g. 1, 2, 3).
 --boldsuffix        Specify the suffix file name for BOLD dtseries data (may 
                     differ across studies depending on processing; e.g. Atlas 
                     or MSMAll)
 --skipframes        Specify the number of initial frames you wish to exclude 
                     from the BOLD QC calculation
 --snronly           Specify if you wish to compute only SNR BOLD QC calculation 
                     and skip image generation <yes/no>. Default is [no]
 
 --bolddata          Specify BOLD data numbers separated by comma or pipe. 
                     E.g. --bolddata='1,2,3,4,5'. This flag is interchangeable 
                     with --bolds or --boldruns to allow more redundancy in 
                     specification.
 
                     Note: If unspecified empty the QC script will by default 
                     look into 
                     /<path_to_study_sessions_folder>/<session_id>/session_hcp.txt 
                     and identify all BOLDs to process
 
 BOLD FC INPUTS
 --------------
 
 Requires --boldfc='<pconn or pscalar>', --boldfcinput=<image_input>, 
 --bolddata or --boldruns or --bolds
 
 --boldfc            Specify if you wish to compute BOLD QC for FC-type BOLD 
                     results. Supported: pscalar or pconn. Default is []
 --boldfcpath        Specify path for input FC data. Default is 
                     [<study_folder>/sessions/<session_id>/images/functional]
 --boldfcinput       Required. If no --boldfcpath is provided then specify only 
                     data input name after bold<Number>_ which is searched for 
                     in <sessions_folder>/<session_id>/images/functional 
 
                     pscalar FC
                        Atlas_hpss_res-mVWMWB_lpss_CAB-NP-718_r_Fz_GBC.pscalar.nii 
                     pconn FC
                        Atlas_hpss_res-mVWMWB_lpss_CAB-NP-718_r_Fz.pconn.nii
 
 
 OPTIONAL QC INPUTS FOR CUSTOM SCENE
 -----------------------------------
 
 --processcustom     Yes or no. Default is [no]. If set to 'yes' then the script 
                     looks into: 
                     ~/<study_path>/processing/scenes/QC/ for additional custom 
                     QC scenes.
 
                     Note: The provided scene has to conform to QuNex QC 
                     template standards.xw
 
                     See $TOOLS/$QUNEXREPO/qx_library/data/scenes/qc/ for example 
                     templates. The qc path has to contain relevant files for 
                     the provided scene.
 --omitdefaults      Yes or no. Default is [no]. If set to 'yes' then the script 
                     omits defaults.
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/run_qc.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
  qunex run_qc --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
                    
  --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
 ::
 
  # -- raw NII QC
  qunex run_qc \ 
  --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separated_list_of_cases>' \ 
  --modality='rawNII' 
 
  # -- T1w QC
  qunex run_qc \ 
  --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separated_list_of_cases>' \ 
  --outpath='<path_for_output_file> \ 
  --scenetemplatefolder='<path_for_the_template_folder>' \ 
  --modality='T1w' \ 
  --overwrite='yes' 
 
  # -- T2w QC
  qunex run_qc \ 
  --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separated_list_of_cases>' \ 
  --outpath='<path_for_output_file> \ 
  --scenetemplatefolder='<path_for_the_template_folder>' \ 
  --modality='T2w' \ 
  --overwrite='yes'
 
  # -- Myelin QC
  qunex run_qc \ 
  --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separated_list_of_cases>' \ 
  --outpath='<path_for_output_file> \ 
  --scenetemplatefolder='<path_for_the_template_folder>' \ 
  --modality='myelin' \ 
  --overwrite='yes'
 
  # -- DWI QC 
  qunex run_qc \ 
  --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separated_list_of_cases>' \ 
  --scenetemplatefolder='<path_for_the_template_folder>' \ 
  --modality='DWI' \ 
  --outpath='<path_for_output_file> \ 
  --dwilegacy='yes' \ 
  --dwidata='<file_name_for_dwi_data>' \ 
  --dwipath='<path_for_dwi_data>' \ 
  --overwrite='yes'
 
  # -- BOLD QC
  qunex run_qc \ 
  --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separated_list_of_cases>' \ 
  --outpath='<path_for_output_file> \ 
  --scenetemplatefolder='<path_for_the_template_folder>' \ 
  --modality='BOLD' \ 
  --bolddata='1' \ 
  --boldsuffix='Atlas' \ 
  --overwrite='yes'
 
  # -- BOLD FC QC [pscalar or pconn]
  qunex run_qc \ 
  --overwritestep='yes' \ 
  --sessionsfolder='<path_to_study_sessions_folder>' \ 
  --sessions='<comma_separated_list_of_cases>' \ 
  --modality='BOLD' \ 
  --boldfc='<pscalar_or_pconn>' \ 
  --boldfcinput='<data_input_for_bold_fc>' \ 
  --bolddata='1' \ 
  --overwrite='yes' 



run_turnkey
-----------

 This function implements QuNex Suite workflows as a turnkey function.
 It operates on a local server or cluster or within the XNAT Docker engine.
 
 
 INPUTS
 ======
 
 --turnkeytype         Specify type turnkey run. Options are: local or xnat.
                       Default: [xnat].
 --path                Path where study folder is located. If empty default 
                       is [/output/xnatprojectid] for XNAT run.
 --sessions            Sessions to run locally on the file system if not an XNAT 
                       run.
 --sessionids          Comma separated list of session IDs to select for a run 
                       via gMRI engine from the batch file.
 --turnkeysteps        Specify specific turnkey steps you wish to run:
                       Supported: ${QuNexTurnkeyWorkflow} 
 --turnkeycleanstep    Specify specific turnkey steps you wish to clean up 
                       intermediate files for.
                       Supported: ${QuNexTurnkeyClean}
 --batchfile           Batch file with pre-configured header specifying 
                       processing parameters
 
                       Note: This file needs to be created *manually* prior to 
                       starting runTurnkey.
 
                       - IF executing a 'local' run then provide the absolute 
                         path to the file on the local file system:
                         If no file name is given then by default QuNex 
                         RunTurnkey will exit with an error.
                       - IF executing a run via the XNAT WebUI then provide the 
                         name of the file. This file should be created and 
                         uploaded manually as the project-level resource on XNAT
 
 --mappingfile         File for mapping NIFTI files into the desired QuNex file 
                       structure (e.g. hcp, fMRIPrep, etc.)
 
                       Note: This file needs to be created *manually* prior to 
                       starting runTurnkey
 
                       - IF executing a 'local' run then provide the absolute 
                         path to the file on the local file system:
                         If no file name is given then by default QuNex 
                         RunTurnkey will exit with an error.
                       - IF executing a run via the XNAT WebUI then provide the 
                         name of the file. This file should be created and 
                         uploaded manually as the project-level resource on XNAT
 
 ACCEPTANCE TESTING INPUT
 ------------------------
 
 --acceptancetest      Specify if you wish to run a final acceptance test after 
                       each unit of processing. Default is [no]
 
                       If --acceptancetest='yes', then --turnkeysteps must be 
                       provided and will be executed first.
 
                       If --acceptancetest='<turnkey_step>', then acceptance 
                       test will be run but step won't be executed.
 
 XNAT HOST, PROJECT AND USER INPUTS
 ----------------------------------
 
 --xnathost            Specify the XNAT site hostname URL to push data to.
 --xnatprojectid       Specify the XNAT site project id. This is the Project ID 
                       in XNAT and not the Project Title.
 --xnatuser            Specify XNAT username.
 --xnatpass            Specify XNAT password.
 
 XNAT SUBJECT AND SESSION INPUTS
 -------------------------------
 
 --xnatsubjectid       ID for subject across the entire XNAT database. 
                       Required or --xnatsubjectlabel needs to be set.
 --xnatsubjectlabel    Label for subject within a project for the XNAT database. 
                       Required or --xnatsubjectid needs to be set.
 --xnataccsessionid    ID for subject-specific session within the XNAT project. 
                       Derived from XNAT but can be set manually.
 --xnatsessionlabel    Label for session within XNAT project. Note: may be 
                       general across multiple subjects (e.g. rest). Required.
 --xnatstudyinputpath  The path to the previously generated session data as 
                       mounted for the container. Default is 
                       [/input/RESOURCES/qunex_session]
 
 MISCELLANEOUS INPUTS
 --------------------
 
 --dataformat            Specify the format in which the data is. Acceptable 
                         values are:
 
                         - DICOM ... datasets with images in DICOM format
                         - BIDS  ... BIDS compliant datasets
                         - HCPLS ... HCP Life Span datasets
                         - HCPYA ... HCP Young Adults (1200) dataset
 
                         Default is [DICOM]
 
 --hcp_filename          Specify how files and folders should be named using HCP 
                         processing:
 
                         automated
                            files should be named using QuNex automated naming 
                            (e.g. BOLD_1_PA)
                         userdefined
                            files should be named using user defined names 
                            (e.g. rfMRI_REST1_AP)
 
                         Note that the filename to be used has to be provided in 
                         the session_hcp.txt file or the standard naming will be 
                         used. If not provided the default 'standard' will be 
                         used.
 --bidsformat            Note: this parameter is deprecated and is kept for 
                         backward compatibility. 
 
                         If set to yes, it will set --dataformat to BIDS. If 
                         left undefined or set to no, the --dataformat value
                         will be used. The specification of the parameter 
                         follows ...
 
                         Specify if input data is in BIDS format (yes/no). 
                         Default is [no]. If set to yes, it overwrites the 
                         --dataformat parameter.
 
                         Note:
 
                         - If --bidsformat='yes' and XNAT run is requested then 
                           --xnatsessionlabel is required.
                         - If --bidsformat='yes' and XNAT run is NOT requested 
                           then BIDS data expected in <sessions_folder/inbox/BIDS
 --bidsname              The name of the BIDS dataset. The dataset level 
                         information that does not pertain to a specific session 
                         will be stored in <projectname>/info/bids/<bidsname>. 
                         If bidsname is not provided, it will be deduced from 
                         the name of the folder in which the BIDS database is 
                         stored or from the zip package name.
 --rawdatainput          If --turnkeytype is not XNAT then specify location of 
                         raw data on the file system for a session. Default is 
                         [] for the XNAT type run as host is used to pull data.
 --workingdir            Specify where the study folder is to be created or 
                         resides. Default is [/output].
 --projectname           Specify name of the project on local file system if 
                         XNAT is not specified.
 --overwritestep         Specify <yes> or <no> for delete of prior workflow 
                         step. Default is [no].
 --overwritesession      Specify <yes> or <no> for delete of prior session run. 
                         Default is [no].
 --overwriteproject      Specify <yes> or <no> for delete of entire project 
                         prior to run. Default is [no].
 --overwriteprojectxnat  Specify <yes> or <no> for delete of entire XNAT project 
                         folder prior to run. Default is [no].
 --cleanupsession        Specify <yes> or <no> for cleanup of session folder 
                         after steps are done. Default is [no].
 --cleanupproject        Specify <yes> or <no> for cleanup of entire project 
                         after steps are done. Default is [no].
 --cleanupoldfiles       Specify <yes> or <no> for cleanup of files that are 
                         older than start of run (XNAT run only). Default is 
                         [no].
 --bolds                 For commands that work with BOLD images this flag 
                         specifies which specific BOLD images to process. The 
                         list of BOLDS has to be specified as a comma or pipe 
                         '|' separated string of bold numbers or bold tags as 
                         they are specified in the session_hcp.txt or batch.txt 
                         file. 
 
                         Example: '--bolds=1,2,rest' would process BOLD run 1, 
                         BOLD run 2 and any other BOLD image that is tagged with 
                         the string 'rest'.
 
                         If the parameter is not specified, the default value 
                         'all' will be used. In this scenario every BOLD image 
                         that is specified in the group batch.txt file for that 
                         session will be processed.
 
                         **Note**: This parameter takes precedence over the 
                         'bolds' parameter in the batch.txt file. Therefore when 
                         RunTurnkey is executed and this parameter is ommitted 
                         the '_bolds' specification in the batch.txt file never 
                         takes effect, because the default value 'all' will take 
                         precedence.
 
 CUSTOM QC INPUTS
 ----------------
 
 --customqc          Yes or no. Default is [no]. If set to 'yes' then the script
                     ooks into: ~/<study_path>/processing/scenes/QC/ for 
                     additional custom QC scenes.
 
                     Note: The provided scene has to conform to QuNex QC 
                     template standards.xw
 
                     See $TOOLS/$QUNEXREPO/qx_library/data/scenes/qc/ for example
                     templates.
 
                     The qc path has to contain relevant files for the provided
                     scene.
 --qcplotimages      Absolute path to images for general_plot_bold_timeseries. See 
                     'qunex general_plot_bold_timeseries' for help. 
 
                     Only set if general_plot_bold_timeseries is requested then this is a 
                     required setting.
 --qcplotmasks       Absolute path to one or multiple masks to use for 
                     extracting BOLD data. See 'qunex general_plot_bold_timeseries' for help. 
 
                     Only set if general_plot_bold_timeseries is requested then this is a 
                     required setting.
 --qcplotelements    Plot element specifications for general_plot_bold_timeseries. See 
                     'qunex general_plot_bold_timeseries' for help. 
 
                     Only set if general_plot_bold_timeseries is requested. If not set then the 
                     default is: ${QCPlotElements}
 
 EXAMPLE USE
 ===========
 
 Run directly via::
 
  ${TOOLS}/${QUNEXREPO}/bash/qx_utilities/run_turnkey.sh \ 
  --<parameter1> --<parameter2> --<parameter3> ... --<parameterN> 
 
 NOTE: --scheduler is not available via direct script call.
 
 Run via:: 
 
  qunex runTurnkey --<parameter1> --<parameter2> ... --<parameterN> 
 
 NOTE: scheduler is available via qunex call.
 
 --scheduler       A string for the cluster scheduler (e.g. LSF, PBS or SLURM) 
                   followed by relevant options
 
 For SLURM scheduler the string would look like this via the qunex call:: 
 
  --scheduler='SLURM,jobname=<name_of_job>,time=<job_duration>,ntasks=<number_of_tasks>,cpus-per-task=<cpu_number>,mem-per-cpu=<memory>,partition=<queue_to_send_job_to>' 
 
 ::
 
  run_turnkey.sh \ 
   --turnkeytype=<turnkey_run_type> \ 
   --turnkeysteps=<turnkey_worlflow_steps> \ 
   --batchfile=<batch_file> \ 
   --overwritestep=yes \ 
   --mappingfile=<mapping_file> \ 
   --xnatsubjectlabel=<XNAT_SUBJECT_LABEL> \ 
   --xnatsessionlabel=<XNAT_SESSION_LABEL> \ 
   --xnatprojectid=<name_of_xnat_project_id> \ 
   --xnathostname=<XNAT_site_URL> \ 
   --xnatuser=<xnat_host_user_name> \ 
   --xnatpass=<xnat_host_user_pass> \ 



xnat_upload_download
--------------------

 This function implements syncing to a specified XNAT server via the CURL REST 
 API.
 
 INPUTS
 ======
 
 --xnatcredentialfile   Specify XNAT credential file name. 
                        Default: ${HOME}/.xnat 
 
                        This file stores the username and password for the XNAT 
                        site. Permissions of this file need to be set to 400. If 
                        this file does not exist the script will prompt you to 
                        generate one using default name. If user provided a file 
                        name but it is not found, this name will be used to 
                        generate new credentials. User needs to provide this 
                        specific credential file for next run, as script by 
                        default expects ${HOME}/.xnat
 --xnatuser             Specify XNAT username required if credential file is not 
                        found
 --xnatpass             Specify XNAT password required if credential file is not 
                        found
 --runtype              Select --runtype='upload' or --runtype='download' 
 
 Local system variables if using QuNex hierarchy:
 
 --studyfolder          Path to study on local file system
 --sessionsfolder       Path to study data folder where the sessions folders 
                        reside
 --sessions             List of sessions to run that are study-specific and 
                        correspond to XNAT database session IDs
 --downloadpath         Specify path to download. Default: 
                        <study_folder>/sessions/inbox/ or 
                        <study_folder>/sessions/inbox/BIDS
 --bidsformat           Specify if XNAT data is in BIDS format (yes/no). Default 
                        is [no].
 
                        If --bidsformat='yes' and XNAT download run is requested 
                        then by default. BIDS data is placed in 
                        <sessions_folder/inbox/BIDS
 
 Local system variables if using generic DICOM location for a single XNAT upload:
 
 --dicompath            Path to folder where the DICOMs reside
 
 XNAT HOST VARIABLES
 -------------------
 
 --xnatprojectid        Specify the XNAT site project id. This is the Project ID 
                        in XNAT and not the Project Title.
 
                        This project should be created on the XNAT Site prior to 
                        upload. If it is not found on the XNAT Site or not 
                        provided then the data will land into the prearchive and 
                        be left unassigned to a project.
                        Please check upon completion and specify assignment 
                        manually.
 --xnathost             Specify the XNAT site hostname URL to push data to.
 
 
 XNAT SUBJECT AND SESSION INPUTS
 -------------------------------
 
 --xnatsubjectlabels     Label for subject within a project for the XNAT 
                         database. Default assumes it matches --sessions.
 
                         If your XNAT database subject label is distinct from 
                         your local server subject id then please supply this 
                         flag.
 
                         Use if your XNAT database has a different set of subject 
                         ids.
 --xnatsessionlabel      Label for session within XNAT project. Note: may be 
                         general across multiple subjects (e.g. rest). Required.
 --niftiupload           Specify <yes> or <no> for NIFTI upload. Default is [no].
 --overwrite             Specify <yes> or <no> for cleanup of prior upload on 
                         the host server. Default is [yes].
 --resetcredentials      Specify <yes> if you wish to reset your XNAT site user 
                         and password. Default is [no]
 
 USE
 ===
 
 Note: To invoke this function you need a credential file in your home folder or 
 provide one using --xnatcredentials parameter or --xnatuser and --xnatpass 
 parameters.
 
 EXAMPLE USE
 ===========
 
 Example for XNAT upload::
 
  xnat_upload_download.sh \ 
  --runtype='upload' \ 
  --sessionsfolder='<path_to_sessions_folder>' \ 
  --sessions='<session_label>' \ 
  --xnatcredentialfile='.somefilename' \ 
  --xnatsessionlabel='<session_label>' \ 
  --xnatprojectid='<xnat_project>' \ 
  --xnathost='<host_url>' 
 
 Example for XNAT download::
 
  xnat_upload_download.sh \ 
  --runtype='download' \ 
  --studyfolder='<path_to_study_folder>' \ 
  --sessionsfolder='<path_to_sessions_folder>' \ 
  --sessions='<xnat_subject_labels>' \ 
  --downloadpath='<optional_download_path>' \ 
  --xnatcredentialfile='.somefilename' \ 
  --xnatsessionlabel='<session_label>' \ 
  --xnatprojectid='<xnat_project>' \ 
  --xnathost='<host_url>' \ 
  --bidsformat='yes' \ 