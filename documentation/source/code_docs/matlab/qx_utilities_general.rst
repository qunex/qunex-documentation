general
=======

.. mat:automodule:: qunex.matlab.qx_utilities.general

general_check_file
------------------
.. mat:autofunction:: general_check_file

general_check_folder
--------------------
.. mat:autofunction:: general_check_folder

general_filename_join
---------------------
.. mat:autofunction:: general_filename_join

general_filename_split
----------------------
.. mat:autofunction:: general_filename_split

general_get_qunex_version
-------------------------
.. mat:autofunction:: general_get_qunex_version

general_normalize_timeseries
----------------------------
.. mat:autofunction:: general_normalize_timeseries

general_parse_options
---------------------
.. mat:autofunction:: general_parse_options

general_print_struct
--------------------
.. mat:autofunction:: general_print_struct

general_read_table
------------------
.. mat:autofunction:: general_read_table

general_report_crash
--------------------
.. mat:autofunction:: general_report_crash

general_write_table
-------------------
.. mat:autofunction:: general_write_table

strjoin
-------
.. mat:autofunction:: strjoin