General Matlab MRI
==================

.. mat:automodule:: qunex.matlab.qx_mri.general

general_check_glm
-----------------
.. mat:autofunction:: general_check_glm

general_cluster_threshold
-------------------------
.. mat:autofunction:: general_cluster_threshold

general_compute_bold_list_stats
-------------------------------
.. mat:autofunction:: general_compute_bold_list_stats

general_compute_bold_stats
--------------------------
.. mat:autofunction:: general_compute_bold_stats

general_compute_group_bold_stats
--------------------------------
.. mat:autofunction:: general_compute_group_bold_stats

general_compute_snr
-------------------
.. mat:autofunction:: general_compute_snr

general_compute_snr_group
-------------------------
.. mat:autofunction:: general_compute_snr_group

general_compute_vertex_normals
------------------------------
.. mat:autofunction:: general_compute_vertex_normals

general_conjunction
-------------------
.. mat:autofunction:: general_conjunction

general_create_assumed_response
-------------------------------
.. mat:autofunction:: general_create_assumed_response

general_create_cifti_mat
------------------------
.. mat:autofunction:: general_create_cifti_mat

general_create_glm_hipass_filter
--------------------------------
.. mat:autofunction:: general_create_glm_hipass_filter

general_create_rgb_overlay
--------------------------
.. mat:autofunction:: general_create_rgb_overlay

general_create_task_regressors
------------------------------
.. mat:autofunction:: general_create_task_regressors

general_downsample_data
-----------------------
.. mat:autofunction:: general_downsample_data

general_extract_glm_volumes
---------------------------
.. mat:autofunction:: general_extract_glm_volumes

general_extract_nuisance
------------------------
.. mat:autofunction:: general_extract_nuisance

general_extract_roi_glm_values
------------------------------
.. mat:autofunction:: general_extract_roi_glm_values

general_extract_roi_values
--------------------------
.. mat:autofunction:: general_extract_roi_values

general_find_peaks
------------------
.. mat:autofunction:: general_find_peaks

general_get_central_vertices
----------------------------
.. mat:autofunction:: general_get_central_vertices

general_get_image_length
------------------------
.. mat:autofunction:: general_get_image_length

general_get_peak_values
-----------------------
.. mat:autofunction:: general_get_peak_values

general_get_roi_parcels
-----------------------
.. mat:autofunction:: general_get_roi_parcels

general_hrf
-----------
.. mat:autofunction:: general_hrf

general_image_conjunction
-------------------------
.. mat:autofunction:: general_image_conjunction

general_image_overlap
---------------------
.. mat:autofunction:: general_image_overlap

general_parcellated2dense
-------------------------
.. mat:autofunction:: general_parcellated2dense

general_plot_bold_timeseries
----------------------------
.. mat:autofunction:: general_plot_bold_timeseries

general_plot_bold_timeseries_list
---------------------------------
.. mat:autofunction:: general_plot_bold_timeseries_list

general_qa_concfile
-------------------
.. mat:autofunction:: general_qa_concfile

general_read_concfile
---------------------
.. mat:autofunction:: general_read_concfile

general_read_event_file
-----------------------
.. mat:autofunction:: general_read_event_file

general_read_file_list
----------------------
.. mat:autofunction:: general_read_file_list

general_upsample_data
---------------------
.. mat:autofunction:: general_upsample_data

nimages
-------
.. mat:autofunction:: nimages