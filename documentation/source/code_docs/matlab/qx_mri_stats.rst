stats
=====

.. mat:automodule:: qunex.matlab.qx_mri.stats

stats_anova_2way_repeated
-------------------------
.. mat:autofunction:: stats_anova_2way_repeated

stats_compute_behavioral_correlations
-------------------------------------
.. mat:autofunction:: stats_compute_behavioral_correlations

stats_p2z
---------
.. mat:autofunction:: stats_p2z

stats_partial_correlation_matrix
--------------------------------
.. mat:autofunction:: stats_partial_correlation_matrix

stats_ttest_dependent
---------------------
.. mat:autofunction:: stats_ttest_dependent

stats_ttest_independent
-----------------------
.. mat:autofunction:: stats_ttest_independent

stats_ttest_zero
----------------
.. mat:autofunction:: stats_ttest_zero
