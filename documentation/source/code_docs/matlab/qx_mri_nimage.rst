nimage
======

.. mat:automodule:: qunex.matlab.qx_mri.img.@nimage

nimage class
------------

.. mat:autoclass:: nimage

Methods
-------

img_anova_2way_repeated
^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_anova_2way_repeated

img_cifti_embed
^^^^^^^^^^^^^^^
.. mat:autofunction:: img_cifti_embed

img_compute_ab_correlation
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_compute_ab_correlation

img_compute_correlations
^^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_compute_correlations

img_compute_gbc
^^^^^^^^^^^^^^^
.. mat:autofunction:: img_compute_gbc

img_compute_gbcd
^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_compute_gbcd

img_compute_r_type1
^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_compute_r_type1

img_compute_r_type3
^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_compute_r_type3

img_compute_scrub
^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_compute_scrub

img_create_roi_from_peaks
^^^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_create_roi_from_peaks

img_embed_cifti_volume
^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_embed_cifti_volume

img_embed_meta
^^^^^^^^^^^^^^
.. mat:autofunction:: img_embed_meta

img_embed_stats
^^^^^^^^^^^^^^^
.. mat:autofunction:: img_embed_stats

img_extract_cifti_volume
^^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_extract_cifti_volume

img_extract_glm_estimates
^^^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_extract_glm_estimates

img_extract_roi
^^^^^^^^^^^^^^^
.. mat:autofunction:: img_extract_roi

img_extract_roi_stats
^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_extract_roi_stats

img_extract_timeseries
^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_extract_timeseries

img_fcmri_segment
^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_fcmri_segment

img_filter
^^^^^^^^^^
.. mat:autofunction:: img_filter

img_find_peaks
^^^^^^^^^^^^^^
.. mat:autofunction:: img_find_peaks

img_find_peaks_surface
^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_find_peaks_surface

img_find_peaks_volume
^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_find_peaks_volume

img_get_central_vertices
^^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_get_central_vertices

img_get_extraction_matrices
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_get_extraction_matrices

img_get_ijk
^^^^^^^^^^^
.. mat:autofunction:: img_get_ijk

img_get_xyz
^^^^^^^^^^^
.. mat:autofunction:: img_get_xyz

img_glm_fit
^^^^^^^^^^^
.. mat:autofunction:: img_glm_fit

img_glm_fit2
^^^^^^^^^^^^
.. mat:autofunction:: img_glm_fit2

img_grow_roi
^^^^^^^^^^^^
.. mat:autofunction:: img_grow_roi

img_mask_roi
^^^^^^^^^^^^
.. mat:autofunction:: img_mask_roi

img_parcellated2dense
^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_parcellated2dense

img_read_4dfp
^^^^^^^^^^^^^
.. mat:autofunction:: img_read_4dfp

img_read_concfile
^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_read_concfile

img_read_concimage
^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_read_concimage

img_read_glm
^^^^^^^^^^^^
.. mat:autofunction:: img_read_glm

img_read_ifh
^^^^^^^^^^^^
.. mat:autofunction:: img_read_ifh

img_read_nifti
^^^^^^^^^^^^^^
.. mat:autofunction:: img_read_nifti

img_read_roi
^^^^^^^^^^^^
.. mat:autofunction:: img_read_roi

img_read_stats
^^^^^^^^^^^^^^
.. mat:autofunction:: img_read_stats

img_roi_mask
^^^^^^^^^^^^
.. mat:autofunction:: img_roi_mask

img_save_4dfp
^^^^^^^^^^^^^
.. mat:autofunction:: img_save_4dfp

img_save_concfile
^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_save_concfile

img_save_nifti
^^^^^^^^^^^^^^
.. mat:autofunction:: img_save_nifti

img_shrink_roi
^^^^^^^^^^^^^^
.. mat:autofunction:: img_shrink_roi

img_slice_matrix
^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_slice_matrix

img_smooth
^^^^^^^^^^
.. mat:autofunction:: img_smooth

img_smooth_3d
^^^^^^^^^^^^^
.. mat:autofunction:: img_smooth_3d

img_smooth_3d_masked
^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_smooth_3d_masked

img_sscrub
^^^^^^^^^^
.. mat:autofunction:: img_sscrub

img_stats
^^^^^^^^^
.. mat:autofunction:: img_stats

img_stats_diff
^^^^^^^^^^^^^^
.. mat:autofunction:: img_stats_diff

img_stats_time
^^^^^^^^^^^^^^
.. mat:autofunction:: img_stats_time

img_ttest_dependent
^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_ttest_dependent

img_ttest_independent
^^^^^^^^^^^^^^^^^^^^^
.. mat:autofunction:: img_ttest_independent

img_ttest_zero
^^^^^^^^^^^^^^
.. mat:autofunction:: img_ttest_zero
