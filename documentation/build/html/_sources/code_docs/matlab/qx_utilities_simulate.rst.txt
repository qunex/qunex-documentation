simulate
========

.. mat:automodule:: qunex.matlab.qx_utilities.simulate

changeform
----------
.. mat:autofunction:: changeform

simulate_compute_glm
--------------------
.. mat:autofunction:: simulate_compute_glm

simulate_extract_event_timepoints
---------------------------------
.. mat:autofunction:: simulate_extract_event_timepoints

simulate_generate_correlated_timeseries
---------------------------------------
.. mat:autofunction:: simulate_generate_correlated_timeseries

simulate_generate_timeseries
----------------------------
.. mat:autofunction:: simulate_generate_timeseries

simulate_run_simulation_ec
--------------------------
.. mat:autofunction:: simulate_run_simulation_ec

simulate_trim_pad
-----------------
.. mat:autofunction:: simulate_trim_pad

simulate_voronoi_map
--------------------
.. mat:autofunction:: simulate_voronoi_map
