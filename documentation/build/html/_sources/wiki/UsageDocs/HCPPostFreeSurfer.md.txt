# HCP PostFreeSurfer pipeline

Prerequisites:

* [HCP FreeSurfer](HCPFreeSurfer.md)

For all HCP pipeline steps please refer to [Running the HCP pipeline](RunningPreprocessHCP.md). For an overview on how to prepare data and run the HCP preprocessing steps, see [Overview of steps for running the HCP pipeline](HCPPreprocessing.md).

The third step of structural image processing is used to perform the following operations:

* conversion of FreeSurfer volumes and surfaces to NIfTI and CIFTI
* creation of FreeSurfer ribbon file at full resolution
* myelin mapping

Within QuNex the HCP pipeline is run using the `hcp_post_freesurfer` command. Do note that the exact steps that are executed depend on parameters specified. In contrast to the original HCP pipeline, the modified version of the pipeline included in QuNex does not require the presence of a T2w image or field maps. The following parameters are relevant for this step and should be set either in a command line or in `batch.txt` file:

``` bash
--hcp_suffix            ... Specifies a suffix to the session id if multiple
                            variants are run, empty otherwise [].

--hcp_t2                ... NONE if no T2w image is available and the
                            preprocessing should be run without them,
                            anything else otherwise [t2]. NONE is only valid
                            if 'LegacyStyleData' processing mode was specified.

--hcp_grayordinatesres  ... The resolution of the volume part of the
                            graordinate representation in mm [2].

--hcp_hiresmesh         ... The number of vertices for the high resolution
                            mesh of each hemisphere (in thousands) [164].

--hcp_lowresmesh        ... The number of vertices for the low resolution
                            mesh of each hemisphere (in thousands) [32].

--hcp_regname           ... The registration used, FS or MSMSulc [MSMSulc].

--hcp_mcsigma           ... Correction sigma used for metric smooting [sqrt(200)].

--hcp_inflatescale      ... Inflate extra scale parameter [1].

--hcp_postfs_check      ... Whether to check the results of PreFreeSurfer 
                            pipeline by presence of last file generated 
                            ('last'), the default list of all files ('all') 
                            or using a specific check file ('<path to file>')
                            ['last']
```

## Examples

``` bash
# Example run from the base study folder with test flag
qunex hcp_post_freesurfer \
     --sessions="processing/batch.hcp.txt" \
     --sessionsfolder="sessions" \
     --parsessions="10" \
     --overwrite="no" \
     --test

# Example run with absolute paths with scheduler
qunex hcp_post_freesurfer \
    --sessions="<path_to_study_folder>/processing/batch.hcp.txt" 
    --sessionsfolder="<path_to_study_folder>/sessions" \
    --parsessions="4" \
    --hcp_t2="NONE" \
    --overwrite="yes" \
    --scheduler="SLURM,time=24:00:00,ntasks=10,cpus-per-task=2,mem-per-cpu=2500,partition=YourPartition"
```
