# Diffusion weighted imaging (DWI) 'legacy' data preprocessing

Prerequisites:

* [HCP PreFreeSurfer](HCPPreFreeSurfer.md)

For all HCP pipeline steps please refer to [Running the HCP pipeline](RunningPreprocessHCP.md). For an overview on how to prepare data and run the HCP preprocessing steps, see [Overview of steps for running the HCP pipeline](HCPPreprocessing.md).

## Overview

Prior to [DWI analyses](https://bitbucket.org/oriadev/qunex/wiki/UsageDocs/DWIAnalyses.md), DWI data must be preprocessed. For multi-shell phase-encoding reversed DWI data please refer to the [HCP preprocessing via QuNex](https://bitbucket.org/oriadev/qunex/wiki/UsageDocs/HCPPreprocessing "QuNex HCP workflow").

If the DWI data has not been phase-encoding reversed and/or is missing a field map (i.e. pre-HCP 'legacy' acquisitions), then use the `dwi_legacy` command.

Specifically, this workflow is a modification of `hcp_diffusion` that is compatible with 'legacy' data, which have been acquired without a pair of phase-encoding direction reversed images.

`dwi_legacy` runs a modified version of the [HCP Diffusion preprocessing pipeline](http://www.sciencedirect.com/science/article/pii/S1053811913005053 "Glasser, 2013"), using the "[eddy](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy "FSL: eddy")" tool to correct for eddy-current induced field inhomogeneities and head motion, and skipping the "[topup](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup "FSL: topup")" step performed in `hcp_diffusion` which would require a pair of phase-encoding reversed images.

## DWI 'legacy' preprocessing: prerequisites and general settings

* `dwi_legacy` can be run after completion of the `hcp_pre_freesurfer` command, and may be run before FreeSurfer (see instructions here for [HCP preprocessing via QuNex](https://bitbucket.org/hidradev/qunex/wiki/UsageDocs/HCPPreprocessing "QuNex HCP workflow")).
* At minimum, the pipeline requires a native-space DWI image (single-shell or multi-shell) and a corresponding T1w image, both organized in the standard HCP file hierarchy within the individual session directories (see [Data Hierarchy Specification](https://bitbucket.org/hidradev/qunex/wiki/Overview/DataHierarchy.md))
* For data collected on a Siemens scanner, a standard set of fieldmap images (magnitude and phase) can be used to better correct for field inhomogeneity distortion. However, if fieldmaps are missing the command will omit them.

The following parameters are relevant for running `dwi_legacy` with or without fieldmaps:

``` bash
--sessionsfolder=<folder_with_sessions>      ... Path to study data folder
--sessions=<list_of_cases>                   ... List of sessions to run
--scanner=<scanner_manufacturer>             ... Name of scanner manufacturer (siemens or ge supported) 
--usefieldmap=<yes_no>                       ... Whether to use the standard field map. If set to <yes> then the TE parameter becomes mandatory. 
--echospacing=<echo_spacing_value>           ... EPI Echo Spacing for data [in msec]; e.g. 0.69
--PEdir=<phase_encoding_direction>           ... Use 1 for Left-Right Phase Encoding, 2 for Anterior-Posterior
--unwarpdir=<epi_phase_unwarping_direction>  ... Direction for EPI image unwarping; e.g. x or x- for LR/RL, y or y- for AP/PA; may need to try out both -/+ combinations
-diffdatasuffix=<diffusion_data_name>        ... Name of the DWI image; e.g. if the data is called <SessionID>_DWI_dir91_LR.nii.gz - you would enter DWI_dir91_LR
--overwrite=<clean_prior_run>                ... Delete prior run for a given subject
```

If using a Siemens fieldmap image, the following option must also be included:

``` bash
--TE=<delta_te_value_for_fieldmap>           ... This is the echo time difference of the fieldmap sequence - find this out form the operator - defaults are *usually* 2.46ms on SIEMENS
```

## Examples

Example with flagged parameters for a local run using Siemens FieldMap [needs GPU-enabled node]

``` bash
qunex dwi_legacy \
    --sessionsfolder='/gpfs/project/fas/n3/Studies/Anticevic.DP5/sessions' \
    --sessions='ta6455' \
    --PEdir='1' \
    --echospacing='0.69' \
    --TE='2.46' \
    --unwarpdir='x-' \
    --diffdatasuffix='DWI_dir91_LR' \
    --usefieldmap='yes' \
    --scanner='siemens' \
    --overwrite='yes'
```

Example with flagged parameters for submission to the scheduler using Siemens FieldMap [needs GPU-enabled queue]

``` bash
qunex dwi_legacy \
    --sessionsfolder='/gpfs/project/fas/n3/Studies/Anticevic.DP5/sessions' \
    --sessions='ta6455' \
    --PEdir='1' \
    --echospacing='0.69' \
    --TE='2.46' \
    --unwarpdir='x-' \
    --diffdatasuffix='DWI_dir91_LR' \
    --scheduler='<name_of_scheduler_and_options>' \
    --usefieldmap='yes' \
    --scanner='siemens' \
    --overwrite='yes'
```

Example with flagged parameters for submission to the scheduler using GE data without FieldMap [needs GPU-enabled queue]

``` bash
qunex dwi_legacy \
    --sessionsfolder='/gpfs/project/fas/n3/Studies/Anticevic.DP5/sessions' \
    --sessions='ta6455' \
    --diffdatasuffix='DWI_dir91_LR' \
    --scheduler='<name_of_scheduler_and_options>' \
    --usefieldmap='no' \
    --PEdir='1' \
    --echospacing='0.69' \
    --unwarpdir='x-' \
    --scanner='ge' \
    --overwrite='yes'
```

For more detailed instructions on how to use the listed commands, please refer to their embedded help (e.g. by running `qunex dwi_legacy`). For information on other aspects of preprocessing, or for non-legacy DWI data, please consult the [HCP preprocessing](https://bitbucket.org/hidradev/qunex/wiki/UsageDocs/HCPPreprocessing "HCP preprocessing workflow") page.