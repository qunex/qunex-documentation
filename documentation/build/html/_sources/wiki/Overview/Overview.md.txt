# General overview

QuNex provides a software platform and ecosystem that collectively supports an extensible framework for data organization, preprocessing, quality assurance and analyses across neuroimaging modalities. QuNex is explicitly architected to enable flexible and extensible addition of new methods and functions developed around its component interconnected modules. It is co-developed and co-maintained by the [Mind and Brain Lab](http://psy.ff.uni-lj.si/mblab/en) and the [Anticevic Lab](http://anticeviclab.yale.edu/).

Collectively, QuNex enables rapid and efficient handling of neuroimaging data across multiple modalities and workflows: from importing of ‘raw’ DICOMs, sequential preprocessing across modalities, more advanced signal processing and GLM task-based analyses, functional and structural connectivity analyses, statistical testing and visualization, etc. QuNex tools are built around a flexible architecture that facilitates massive parallelization across multiple cores of a single machine or across multiple nodes on a high-performance cluster or in the cloud. This in turn enables rapid throughput of large-scale datasets. In summary, the QuNex ecosystem is purpose-built for flexible, extensible and rapid analytic throughput.

QuNex code is developed in several programming languages (Python, Bash, MATLAB, R). The code inside the repository is split by using programming language on the root level and by function on the next level. Folder, file and command names are all in `snake_case`. The main QuNex repository includes a library submodule called `qx_library`, which provides support files, libraries and data templates (e.g. atlases) used across QuNex.

## QuNex container overview

QuNex is available via container; it supports both Docker and Singularity versions. The container contains the entire suite mentioned above (all modules) and also includes all the necessary dependencies. For a full list of dependencies please see [Installation and Dependencies](https://bitbucket.org/oriadev/qunex/wiki/Overview/Installation.md). Containerization enables access to a clearly versioned, acceptance tested, self-contained unit that promotes easy deployment on desktops, servers, clusters and in the cloud, as well as processing and analysis replicability.

## Accessing in-line documentation

To get the version of the QuNex suite you are using run:

``` bash
qunex --version
```

A list of all functions can be obtained by running the following:

``` bash
qunex -help
```

The general `qunex` call use form is:

``` bash
qunex --command="<command_name>" --option="<value>" --option="<value>" ...
```

The simplified form omits the command flag:

``` bash
qunex <command_name> --option="<value>" --option="<value>" ...
```

The list of commands and their specific documentation can be acquired by running `qunex`.

To get help for a specific command use the following call:

``` bash
qunex ?<command_name>
```

Please note the following conventions used:

* Angle brackets `<>` describe the value that should be provided.
* Square brackets `[]` denote an option or argument that is optional. The
  value listed in the brackets is the default value used, if the argument
  is not explicitly specified.
* Dashes or "flags", `-` in the documentation define input variables.
* Command names, arguments, and option names are either in small or "camel" case.
* Use descriptions are in regular "sentence" case.
* Option values are usually specified in capital case (e.g. `YES`, `NONE`).