# Installing and enabling GNU Octave

GNU Octave can be installed as a command line program or as a full GUI application. The command line program is sufficient for working with the QuNex suite. The GUI application might be more practical for developers.

GNU Octave can be installed by following the instructions from Octave's download page: [https://www.gnu.org/software/octave/download.html](https://www.gnu.org/software/octave/download.html).

After octave is installed, you will need to complete three additional steps to enable the use of Octave:

1. Installation of required packages
2. Compilation of `.mex` files
3. Setting up the environment

## Installing required packages

All GNU Octave package operations are performed with the Octave's command *pkg*.

QuNex suite requires the following packages to be installed:

* control-3.1.0
* general-2.1.0
* image-2.8.0
* io-2.4.11
* nan-3.1.4
* signal-1.4.0
* statistics-1.4.0
* struct-1.0.15
* optim-1.5.2
* miscellaneous-1.2.1

If your computer is connected to the internet, run the following commands within `octave`:

``` bash
pkg install -forge control
pkg install -forge general
pkg install -forge image
pkg install -forge io
pkg install -forge nan
pkg install -forge signal
pkg install -forge statistics
pkg install -forge struct
pkg install -forge optim
pkg install -forge miscellaneous
```

If you cannot install packages to the global location, use the following instructions: first, create a `$TOOLS/octavepkg` folder in terminal (e.g. `mkdir $TOOLS/octavepkg`), then run the following in `octave` (replace `<TOOLS>` with the location of your $TOOLS folder):

``` bash
pkg prefix <TOOLS>/octavepkg
pkg local_list <TOOLS>/octavepkg/packages

pkg install -local -forge control
pkg install -local -forge general
pkg install -local -forge image
pkg install -local -forge io
pkg install -local -forge nan
pkg install -local -forge signal
pkg install -local -forge statistics
pkg install -local -forge struct
pkg install -local -forge optim
pkg install -local -forge miscellaneous
```

In case the computer cannot connect to the internet and download the latest copy of the packages during the installation, then follow these steps:

1. Download the packages from the Octave Forge website ([https://octave.sourceforge.io/packages.php](https://octave.sourceforge.io/packages.php))
2. Start octave in the folder where you have downloaded the packages
3. Run the following package installation commands:

``` bash
pkg install control-3.1.0.tar.gz
pkg install general-2.1.0.tar.gz
pkg install image-2.8.0.tar.gz
pkg install io-2.4.11.tar.gz
pkg install nan-3.1.4.tar.gz
pkg install signal-1.4.0.tar.gz
pkg install statistics-1.4.0.tar.gz
pkg install struct-1.0.15.tar.gz
pkg install optim-1.5.2.tar.gz
pkg install miscellaneous-1.2.1.tar.gz
```

Again, if you cannot install packages to the global location, first, create a `$TOOLS/octavepkg` folder in terminal (e.g. `mkdir $TOOLS/octavepkg`), then run the following in `octave` (replace `<TOOLS>` with the location of your $TOOLS folder):

``` bash
pkg prefix <TOOLS>/octavepkg
pkg local_list <TOOLS>/octavepkg/packages

pkg install -local control-3.1.0.tar.gz
pkg install -local general-2.1.0.tar.gz
pkg install -local image-2.8.0.tar.gz
pkg install -local io-2.4.11.tar.gz
pkg install -local nan-3.1.4.tar.gz
pkg install -local signal-1.4.0.tar.gz
pkg install -local statistics-1.4.0.tar.gz
pkg install -local struct-1.0.15.tar.gz
pkg install -local optim-1.5.2.tar.gz
pkg install -local miscellaneous-1.2.1.tar.gz
```

You can verify that all the packages were successfully installed by running the following command in `octave`:

``` bash
pkg list
```

More information about GNU Octave packages can be found at [https://www.gnu.org/software/octave/doc/v4.2.0/Installing-and-Removing-Packages.html)](https://www.gnu.org/software/octave/doc/v4.2.0/Installing-and-Removing-Packages.html).

**Please note**: Packages consist of `.m` files and `.oct` files, the latter being functions written in C that are dynamically added as functions when the installed packages are loaded. Whereas `.m` files are stored in the designated local folder (e.g. `$TOOLS/octavepkg/packages`), `.oct` files are stored in the `lib` subfolders of the Octave installation. For that reason, all the packages need to be reinstalled when upgrading to a new version of Octave.

## Compiling .mex (MATLAB Executable) Files

In order to compile .mex files on Debian-based Linux operating systems you may also need to install **liboctave-dev** package first. This is accomplished by the following command ran from the shell:

``` bash
sudo apt-get install liboctave-dev
```

As of 6/19/2018, QuNex suite requires .mex files for the following two functions:

* img_read_nifti_mx
* img_save_nifti_mx

To compile them follow these steps:

1. Change the working directory to `<QuNex folder>/matlab/qx_mri/img/@nimage`  
   `cd $TOOLS/$QUNEXREPO/matlab/qx_mri/img/@nimage`

2. Prepare files for compilation by running:  
    `cp img_read_nifti_mx_octave.cpp img_read_nifti_mx.cpp`
    `cp img_save_nifti_mx_octave.cpp img_save_nifti_mx.cpp`  
3. Compile `.mex` files by running in Octave:  
    `mkoctfile --mex -lz -std=c++11 img_read_nifti_mx.cpp qx_nifti.c znzlib.c`  
    `mkoctfile --mex -lz -std=c++11 img_save_nifti_mx.cpp qx_nifti.c znzlib.c`

4. Remove temporary files by running the following commands in shell:

``` bash
rm img_read_nifti_mx.cpp
rm img_save_nifti_mx.cpp
rm qx_nifti.o 
rm znzlib.o
```

For more information about .mex files for GNU Octave visit [https://www.gnu.org/software/octave/doc/interpreter/Mex_002dFiles.html](https://www.gnu.org/software/octave/doc/interpreter/Mex_002dFiles.html).

## Setting up the environment

To enable Octave to be used by QuNex, first, if the octave itself is not installed in the path defined above, create a symlink to it:

``` bash
ln -s `which octave` $TOOLS/octave
```

Second, create a `.qunexuseoctave` file in your home folder by running the following commands in shell:

``` bash
cd
touch .qunexuseoctave
```

When you log into the system the next time, `qunex_environment.sh` will copy a `.octaverc` file to your home folder (this will automatically add the relevant paths and load the necessary packages each time Octave is run), and set QuNex to use Octave when running MATLAB commands and functions files.