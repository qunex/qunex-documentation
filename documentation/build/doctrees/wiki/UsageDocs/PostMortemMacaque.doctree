���O      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h� Post-mortem macaque tractography�h]�h	�Text���� Post-mortem macaque tractography�����}�(h� Post-mortem macaque tractography��parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h�line�Khh�source��k/Applications/Matlab_Toolboxes/qunex-documentation/documentation/source/wiki/UsageDocs/PostMortemMacaque.md�ubh	�	paragraph���)��}�(h�wQuNex support processing of post-mortem macaque tractography. Below are the steps included in this processing pipeline.�h]�h�wQuNex support processing of post-mortem macaque tractography. Below are the steps included in this processing pipeline.�����}�(h�wQuNex support processing of post-mortem macaque tractography. Below are the steps included in this processing pipeline.�hh/hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*Khhhhh+h,ubh)��}�(hhh]�(h)��}�(h�Study creation�h]�h�Study creation�����}�(h�Study creation�hhAubah}�(h]�h!]�h#]�h%]�h']�uh)hh*Khh>h+h,ubh.)��}�(h�DJust like any other processing we have to start by creating a study:�h]�h�DJust like any other processing we have to start by creating a study:�����}�(h�DJust like any other processing we have to start by creating a study:�hhPhhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*Khh>hhh+h,ubh	�literal_block���)��}�(h�9gmri create_study \
    --studyFolder=/data/macaque_study�h]�h�9gmri create_study \
    --studyFolder=/data/macaque_study�����}�(hhhhaubah}�(h]�h!]�h#]�h%]�h']��language��bash��	xml:space��preserve�uh)h_hh>hhh+h,h*K ubeh}�(h]��study-creation�ah!]�h#]��study creation�ah%]�h']�uh)h
h*Khhhhh+h,ubh)��}�(hhh]�(h)��}�(h�Importing the data into QuNex�h]�h�Importing the data into QuNex�����}�(h�Importing the data into QuNex�hh~ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*Khh{h+h,ubh.)��}�(h��For onboarding post-mortem macaque data QuNex uses a specialized function called import_nhp. The import_nhp is capable of importing folders and archives (.zip and .tar) with data. The visualization below depicts how the input data should be structured.�h]�(h�QFor onboarding post-mortem macaque data QuNex uses a specialized function called �����}�(h�QFor onboarding post-mortem macaque data QuNex uses a specialized function called �hh�hhh+Nh*Nubh	�literal���)��}�(h�
import_nhp�h]�h�
import_nhp�����}�(hhhh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhh+h,h*K ubh�. The �����}�(h�. The �hh�hhh+Nh*Nubh�)��}�(h�
import_nhp�h]�h�
import_nhp�����}�(hhhh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhh+h,h*K ubh�/ is capable of importing folders and archives (�����}�(h�/ is capable of importing folders and archives (�hh�hhh+Nh*Nubh�)��}�(h�.zip�h]�h�.zip�����}�(hhhh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhh+h,h*K ubh� and �����}�(h� and �hh�hhh+Nh*Nubh�)��}�(h�.tar�h]�h�.tar�����}�(hhhh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhh+h,h*K ubh�U) with data. The visualization below depicts how the input data should be structured.�����}�(h�U) with data. The visualization below depicts how the input data should be structured.�hh�hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*Khh{hhh+h,ubh`)��}�(hX  sessions
|
├─ session1
|  └─ dMRI
|     ├─ bvals
|     ├─ bvecs
|     ├─ data.nii.gz
|     └─ nodif_brain_mask.nii.gz
|   
└─ session2
   └─ dMRI
      ├─ bvals
      ├─ bvecs
      ├─ data.nii.gz
      └─ nodif_brain_mask.nii.gz�h]�hX  sessions
|
├─ session1
|  └─ dMRI
|     ├─ bvals
|     ├─ bvecs
|     ├─ data.nii.gz
|     └─ nodif_brain_mask.nii.gz
|   
└─ session2
   └─ dMRI
      ├─ bvals
      ├─ bvecs
      ├─ data.nii.gz
      └─ nodif_brain_mask.nii.gz�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hh{hhh+h,h*K ubh.)��}�(h�5The following parameters are relevant for import_nhp:�h]�(h�*The following parameters are relevant for �����}�(h�*The following parameters are relevant for �hh�hhh+Nh*Nubh�)��}�(h�
import_nhp�h]�h�
import_nhp�����}�(hhhj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�hhh+h,h*K ubh�:�����}�(h�:�hh�hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*K$hh{hhh+h,ubh`)��}�(hX�  --sessionsfolder    The sessions folder where all the sessions are to be 
                    mapped to. It should be a folder within the 
                    <study folder>. [.]

--inbox             The location of the NHP dataset. It can be a folder
                    that contains the NHP datasets or compressed `.zip`
                    or `.tar.gz` packages that contain a single 
                    session or a multi-session dataset. For instance the user 
                    can specify "<path>/<nhp_file>.zip" or "<path>" to
                    a folder that contains multiple packages. The default 
                    location where the command will look for a NHP dataset
                    is [<sessionsfolder>/inbox/NHP].

--sessions          An optional parameter that specifies a comma or pipe
                    separated list of sessions from the inbox folder to be 
                    processed. Regular expression patterns can be used. 
                    If provided, only packets or folders within the inbox 
                    that match the list of sessions will be processed. If 
                    `inbox` is a file `sessions` will not be applied.
                    Note: the session will match if the string is found
                    within the package name or the session id. So 
                    "NHP" with match any zip file that contains string
                    "NHP" or any session id that contains "NHP"!

--action            How to map the files to QuNex structure. ["link"]
                    The following actions are supported:
                    
                    - link (files will be mapped by creating hard links if 
                      possible, otherwise they will be copied)
                    - copy (files will be copied)
                    - move (files will be moved)

--overwrite         The parameter specifies what should be done with 
                    data that already exists in the locations to which NHP
                    data would be mapped to. ["no"] Options are:

                    - no (do not overwrite the data and skip processing of 
                      the session)
                    - yes (remove existing files in `nii` folder and redo the
                      mapping) 

--archive           What to do with the files after they were mapped. 
                    ["move"] Options are:

                    - leave (leave the specified archive where it is)
                    - move (move the specified archive to 
                      `<sessionsfolder>/archive/NHP`)
                    - copy (copy the specified archive to 
                      `<sessionsfolder>/archive/NHP`)
                    - delete (delete the archive after processing if no 
                      errors were identified)

                    Please note that there can be an
                    interaction with the `action` parameter. If files are
                    moved during action, they will be missing if `archive` 
                    is set to "move" or "copy".�h]�hX�  --sessionsfolder    The sessions folder where all the sessions are to be 
                    mapped to. It should be a folder within the 
                    <study folder>. [.]

--inbox             The location of the NHP dataset. It can be a folder
                    that contains the NHP datasets or compressed `.zip`
                    or `.tar.gz` packages that contain a single 
                    session or a multi-session dataset. For instance the user 
                    can specify "<path>/<nhp_file>.zip" or "<path>" to
                    a folder that contains multiple packages. The default 
                    location where the command will look for a NHP dataset
                    is [<sessionsfolder>/inbox/NHP].

--sessions          An optional parameter that specifies a comma or pipe
                    separated list of sessions from the inbox folder to be 
                    processed. Regular expression patterns can be used. 
                    If provided, only packets or folders within the inbox 
                    that match the list of sessions will be processed. If 
                    `inbox` is a file `sessions` will not be applied.
                    Note: the session will match if the string is found
                    within the package name or the session id. So 
                    "NHP" with match any zip file that contains string
                    "NHP" or any session id that contains "NHP"!

--action            How to map the files to QuNex structure. ["link"]
                    The following actions are supported:
                    
                    - link (files will be mapped by creating hard links if 
                      possible, otherwise they will be copied)
                    - copy (files will be copied)
                    - move (files will be moved)

--overwrite         The parameter specifies what should be done with 
                    data that already exists in the locations to which NHP
                    data would be mapped to. ["no"] Options are:

                    - no (do not overwrite the data and skip processing of 
                      the session)
                    - yes (remove existing files in `nii` folder and redo the
                      mapping) 

--archive           What to do with the files after they were mapped. 
                    ["move"] Options are:

                    - leave (leave the specified archive where it is)
                    - move (move the specified archive to 
                      `<sessionsfolder>/archive/NHP`)
                    - copy (copy the specified archive to 
                      `<sessionsfolder>/archive/NHP`)
                    - delete (delete the archive after processing if no 
                      errors were identified)

                    Please note that there can be an
                    interaction with the `action` parameter. If files are
                    moved during action, they will be missing if `archive` 
                    is set to "move" or "copy".�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hh{hhh+h,h*K ubh.)��}�(h�]If the data onboarding process was successful dMRI images for each session will be stored in:�h]�h�]If the data onboarding process was successful dMRI images for each session will be stored in:�����}�(h�]If the data onboarding process was successful dMRI images for each session will be stored in:�hj,  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*Kahh{hhh+h,ubh`)��}�(h�<sessionsfolder>/<session>/dMRI�h]�h�<sessionsfolder>/<session>/dMRI�����}�(hhhj;  ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hh{hhh+h,h*K ubh.)��}�(h�#Below is an example of data import:�h]�h�#Below is an example of data import:�����}�(h�#Below is an example of data import:�hjK  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*Kghh{hhh+h,ubh`)��}�(h��gmri import_nhp \
    --sessionsfolder=/data/macaque_study/sessions \
    --inbox=/data/macaque_raw \
    --archive=leave \
    --overwrite=yes�h]�h��gmri import_nhp \
    --sessionsfolder=/data/macaque_study/sessions \
    --inbox=/data/macaque_raw \
    --archive=leave \
    --overwrite=yes�����}�(hhhjZ  ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hh{hhh+h,h*K ubeh}�(h]��importing-the-data-into-qunex�ah!]�h#]��importing the data into qunex�ah%]�h']�uh)h
h*Khhhhh+h,ubh)��}�(hhh]�(h)��}�(h�DTIFIT�h]�h�DTIFIT�����}�(h�DTIFIT�hju  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*Kqhjr  h+h,ubh.)��}�(h�SThe next processing step in this pipeline is FSL's DTIFIT (detailed documentation):�h]�(h�0The next processing step in this pipeline is FSL�����}�(h�0The next processing step in this pipeline is FSL�hj�  hhh+Nh*Nubh�’�����}�(h�'�hj�  hhh+Nh*Nubh�
s DTIFIT (�����}�(h�
s DTIFIT (�hj�  hhh+Nh*Nubh	�	reference���)��}�(h�detailed documentation�h]�h�detailed documentation�����}�(h�detailed documentation�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']��refuri��>https://bitbucket.org/oriadev/qunex/wiki/UsageDocs/DWIAnalyses�uh)j�  h*Kshj�  hhh+h,ubh�):�����}�(h�):�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*Kshjr  hhh+h,ubh`)��}�(h�}qunex dwi_dtifit \
    --sessionsfolder=/data/macaque_study/sessions \
    --sessions="hilary,jane" \
    --species="macaque"�h]�h�}qunex dwi_dtifit \
    --sessionsfolder=/data/macaque_study/sessions \
    --sessions="hilary,jane" \
    --species="macaque"�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hjr  hhh+h,h*K ubeh}�(h]��dtifit�ah!]�h#]��dtifit�ah%]�h']�uh)h
h*Kqhhhhh+h,ubh)��}�(hhh]�(h)��}�(h�BEDPOSTX�h]�h�BEDPOSTX�����}�(h�BEDPOSTX�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*K|hj�  h+h,ubh.)��}�(h�;After DTIFIT comes FSL's BEDPOSTX (detailed documentation):�h]�(h�After DTIFIT comes FSL�����}�(h�After DTIFIT comes FSL�hj�  hhh+Nh*Nubh�’�����}�(hj�  hj�  hhh+Nh*Nubh�s BEDPOSTX (�����}�(h�s BEDPOSTX (�hj�  hhh+Nh*Nubj�  )��}�(h�detailed documentation�h]�h�detailed documentation�����}�(h�detailed documentation�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�j�  �>https://bitbucket.org/oriadev/qunex/wiki/UsageDocs/DWIAnalyses�uh)j�  h*K~hj�  hhh+h,ubh�):�����}�(h�):�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*K~hj�  hhh+h,ubh`)��}�(hX  qunex dwi_bedpostx_gpu \
    --sessionsfolder=/data/macaque_study/sessions \
    --sessions="hilary,jane" \
    --species="macaque" \
    --bash="module load CUDA/9.1.85" \
    --scheduler="SLURM,time=12:00:00,ntasks=1,cpus-per-task=1,mem-per-cpu=16000,gres=gpu:1,jobname=qx_bedpostx"�h]�hX  qunex dwi_bedpostx_gpu \
    --sessionsfolder=/data/macaque_study/sessions \
    --sessions="hilary,jane" \
    --species="macaque" \
    --bash="module load CUDA/9.1.85" \
    --scheduler="SLURM,time=12:00:00,ntasks=1,cpus-per-task=1,mem-per-cpu=16000,gres=gpu:1,jobname=qx_bedpostx"�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hj�  hhh+h,h*K ubh.)��}�(h��Since QuNex uses FSL's GPU BEDPOSTX implementation the example above schedules the command for execution on a GPU node. It also loads the appropriate CUDA module through the bash parameter.�h]�(h�Since QuNex uses FSL�����}�(h�Since QuNex uses FSL�hj  hhh+Nh*Nubh�’�����}�(hj�  hj  hhh+Nh*Nubh��s GPU BEDPOSTX implementation the example above schedules the command for execution on a GPU node. It also loads the appropriate CUDA module through the �����}�(h��s GPU BEDPOSTX implementation the example above schedules the command for execution on a GPU node. It also loads the appropriate CUDA module through the �hj  hhh+Nh*Nubh�)��}�(h�bash�h]�h�bash�����}�(hhhj.  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj  hhh+h,h*K ubh� parameter.�����}�(h� parameter.�hj  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*K�hj�  hhh+h,ubeh}�(h]��bedpostx�ah!]�h#]��bedpostx�ah%]�h']�uh)h
h*K|hhhhh+h,ubh)��}�(hhh]�(h)��}�(h�F99 registration�h]�h�F99 registration�����}�(h�F99 registration�hjR  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*K�hjO  h+h,ubh.)��}�(h�kNext, we use the dwi_f99 (detailed documentation) QuNex command to register our diffusion to the F99 atlas:�h]�(h�Next, we use the �����}�(h�Next, we use the �hja  hhh+Nh*Nubh�)��}�(h�dwi_f99�h]�h�dwi_f99�����}�(hhhjj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hja  hhh+h,h*K ubh� (�����}�(h� (�hja  hhh+Nh*Nubj�  )��}�(h�detailed documentation�h]�h�detailed documentation�����}�(h�detailed documentation�hj}  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�j�  �>https://bitbucket.org/oriadev/qunex/wiki/UsageDocs/DWIAnalyses�uh)j�  h*K�hja  hhh+h,ubh�;) QuNex command to register our diffusion to the F99 atlas:�����}�(h�;) QuNex command to register our diffusion to the F99 atlas:�hja  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*K�hjO  hhh+h,ubh`)��}�(h�`qunex dwi_f99 \
    --sessionsfolder=/data/macaque_study/sessions \
    --sessions="jane,hilary"�h]�h�`qunex dwi_f99 \
    --sessionsfolder=/data/macaque_study/sessions \
    --sessions="jane,hilary"�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hjO  hhh+h,h*K ubeh}�(h]��f99-registration�ah!]�h#]��f99 registration�ah%]�h']�uh)h
h*K�hhhhh+h,ubh)��}�(hhh]�(h)��}�(h�XTRACT tractography�h]�h�XTRACT tractography�����}�(h�XTRACT tractography�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)hh*K�hj�  h+h,ubh.)��}�(h�JFinally, we can use dwi_xtract (detailed documentation) to get the tracts:�h]�(h�Finally, we can use �����}�(h�Finally, we can use �hj�  hhh+Nh*Nubh�)��}�(h�
dwi_xtract�h]�h�
dwi_xtract�����}�(hhhj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj�  hhh+h,h*K ubh� (�����}�(h� (�hj�  hhh+Nh*Nubj�  )��}�(h�detailed documentation�h]�h�detailed documentation�����}�(h�detailed documentation�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�j�  �>https://bitbucket.org/oriadev/qunex/wiki/UsageDocs/DWIAnalyses�uh)j�  h*K�hj�  hhh+h,ubh�) to get the tracts:�����}�(h�) to get the tracts:�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*K�hj�  hhh+h,ubh`)��}�(h��qunex dwi_xtract \
    --sessionsfolder=/gpfs/project/fas/n3/Studies/MBLab/HCPDev/jd_tests/macaque_study/sessions \
    --sessions="jane,hilary" \
    --species="macaque"�h]�h��qunex dwi_xtract \
    --sessionsfolder=/gpfs/project/fas/n3/Studies/MBLab/HCPDev/jd_tests/macaque_study/sessions \
    --sessions="jane,hilary" \
    --species="macaque"�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��language��bash�hqhruh)h_hj�  hhh+h,h*K ubeh}�(h]��xtract-tractography�ah!]�h#]��xtract tractography�ah%]�h']�uh)h
h*K�hhhhh+h,ubeh}�(h]�� post-mortem-macaque-tractography�ah!]�h#]�� post-mortem macaque tractography�ah%]�h']�uh)h
h*Khhhhh+h,ubah}�(h]�h!]�h#]�h%]�h']��source�h,uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j<  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j  j  hxhujo  jl  j�  j�  jL  jI  j�  j�  j  j  u�	nametypes�}�(j  NhxNjo  Nj�  NjL  Nj�  Nj  Nuh}�(j  hhuh>jl  h{j�  jr  jI  j�  j�  jO  j  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.