![QuNex](Images/QuNex_Logo_pantheonsite.png)

# QuNex documentation

## Background

The Quantitative Neuroimaging Environment & ToolboX ([QuNex](https://qunex.yale.edu)) integrates several packages that support a flexible and extensible framework for data organization, preprocessing,
quality assurance, and various analyses across neuroimaging modalities. The QuNex suite is flexible and can be updated by adding specific functions and units of code to support new command calls developed around its component tools.

QuNex suite is developed and maintained by the:

* [Anticevic Lab, Yale University](http://anticeviclab.yale.edu/),
* [Mind and Brain Lab, University of Ljubljana](http://psy.ff.uni-lj.si/mblab/en),
* [Murray Lab, Yale University](https://medicine.yale.edu/lab/murray/).

---

## Forum and issue reporting

You can find our official QuNex forum at [https://forum.qunex.yale.edu/](https://forum.qunex.yale.edu/). Feel free to open up a discussion about anything QuNex related there. The forum is also the official QuNex issue reporting platform. If you are struggling with running a command and cannot find the help in the documentation open a post on the forum. To maintain an effective issue resolving workflow and make sure your problems will be solved in a timely manner we prepared [simple guide](https://forum.qunex.yale.edu/t/qu-nex-issue-reporting-guide-the-dcl-protocol/27).

---

## [QuNex quick start](Overview/QuickStart.md)

Quick start on deploying the QuNex suite starting from raw data to launching HCP pipelines.

---

## [General overview](Overview/Overview.md)

QuNex overview describes general functionality and specifications.

* [Installing from source and dependencies](Overview/Installation.md)  

* [QuNex container deployment](Overview/QuNexContainerUsage.md)  

* [QuNex commands and general usage overview](Overview/GeneralUse.md)  

* [File formats overview](Overview/FileFormats.md)  

* [QuNex data hierarchy specification](Overview/DataHierarchy.md)  

* [Logging and log files](Overview/Logging.md)  

* [Visual schematic of QuNex steps](Overview/VisualSchematic.md)  

* [BIDS support within QuNex](Overview/QuNexBIDS.md)  

* [How to report issues with QuNex](Overview/QuNexIssues.md)  

---

## [User guides](UsageDocs/Overview.md)

User guides provides in-depth description of QuNex usage, detailing specific functions, workflows and use cases.

### Executing QuNex commands

* [Running commands against a container using qunex_container](UsageDocs/RunningQunexContainer.md)  

* [QuNex 'turnkey' workflow and 'batch' engine](UsageDocs/Turnkey.md)  

* [Running lists of QuNex commands](UsageDocs/RunningListsOfCommands.md)  

* [Running commands over multiple sessions](UsageDocs/RunningMultipleSessions.md)

* [Parallel Execution and Scheduling of QuNex commands](UsageDocs/Scheduling.md)  

### Onboarding data into QuNex

* [De-identifying DICOM files](UsageDocs/DICOMDeidentification.md)  

* [Preparing the QuNex study folder hierarchy](UsageDocs/PreparingStudy.md)  

* [Onboarding new data into QuNex](UsageDocs/OnboardingNewData.md)  

### Preparing mapping and batch files

* [Preparing a study-level mapping file for QuNex workflows](UsageDocs/PreparingMappingFile.md)  

* [Preparing study-level batch file and parameters for QuNex workflows](UsageDocs/GeneratingBatchFiles.md)  

### Running HCP pipelines and performing QC

* [Preparing and running the HCP pipeline](UsageDocs/HCPPreprocessing.md)  

* [Running quality control (QC) across modalities](UsageDocs/MultiModalQC.md)

### BOLD preprocessing & analyses

* [BOLD task-evoked and resting-state functional connectivity preprocessing](UsageDocs/BOLDPreprocessing.md)  

* [BOLD movement scrubbing](UsageDocs/MovementScrubbing.md)  

* [Running 1st and 2nd level BOLD task activation analyses](UsageDocs/BOLDTaskActivation.md)  

* [Running BOLD functional connectivity analyses](UsageDocs/BOLDFunctionalConnectivity.md)  

* [Group-level statistics and mapping of results](UsageDocs/GroupLevelStats.md)  

* [Identifying empirical ROI and extracting peak information](UsageDocs/ROIanalyses.md)

### DWI preprocessing & analyses

* [Diffusion weighted imaging (DWI) 'legacy' data preprocessing](UsageDocs/DWILegacyPreprocessing.md)

* [Running DWI analyses](UsageDocs/DWIAnalyses.md)  

### Data visualization & output mapping

* [Visualizing data via CIFTI templates](UsageDocs/CIFTIVisualizationTemplate.md)  

* [Mapping processed data in and out of QuNex](UsageDocs/MappingDataInAndOutQuNex.md)  

### Non human primate (NHP) pipelines

* [Post-mortem macaque tractography](UsageDocs/PostMortemMacaque.md)  

---

## QuNex deployment in XNAT

Overview of interacting with the XNAT environment and executing QuNex container using the XNAT container service.

* [Using the XNAT upload assistant](XNAT/UsingXNATUploadAssistant.md)  

* [QuNex-XNAT integration via container service](XNAT/QuNex_XNAT.md)  

---

## QuNex development

To support development of QuNex, we have prepared [QuNex SDK](https://bitbucket.org/oriadev/qunexsdk), a separate set of tools provided as a Git repository created to support the development and acceptance testing procedures for core QuNex developers as well as others who wish to contribute to the QuNex codebase.

For documentation related to development, please use the [QuNex SDK Wiki](https://bitbucket.org/oriadev/qunexsdk/wiki/Home).
