# Using the XNAT upload assistant

Download and install the XNAT Upload Assistant [here](https://download.xnat.org/upload-assistant/). The XNAT UA copies data from the local storage to the XNAT server.

**STEP 3A**: Configure the XNAT Upload Assistant to communicate with the XNAT site.  

![XNAT UA](https://bitbucket.org/oriadev/qunex/wiki/Images/image_32.png)

**STEP 3B**: Provide the XNAT Site URL and your XNAT Username. **_→_** Click on **Next**.  

![XNAT URL](https://bitbucket.org/oriadev/qunex/wiki/Images/image_33.png)

**STEP 3C**: Once the XNAT UA is configured, select the Project to which data is to be uploaded.  

![Project](https://bitbucket.org/oriadev/qunex/wiki/Images/image_34.png)

**STEP 3D**: Select the Subject to upload. (If a subject does not exist, use **Create new subject**, to create a new subject.)

**STEP 3E**: Set Visit details.  

![Details](https://bitbucket.org/oriadev/qunex/wiki/Images/image_35.png)

Note: If  you do not know the date of the imaging session, select **I don’t know the date or my session doesn't have a date.**

**STEP 3F**: Click → **Next**

**STEP 3G**: Select files.

**STEP 3H**: Verify the selected session/scans.  

![Verify](https://bitbucket.org/oriadev/qunex/wiki/Images/image_36.png)

**STEP 3I**: Click **→ Finish** to upload the session to the project.

The imaging session label is expected to be unique within a project. If for some reason, you upload the same session twice, via any upload channel, XNAT would keep the data in the pre-archive and mark it to be in the **conflict** state.

![Conflict](https://bitbucket.org/oriadev/qunex/wiki/Images/image_37.png)

When a session is in the **conflict** state, click on **Details** and select **Modify and Archive** to assign a new label
