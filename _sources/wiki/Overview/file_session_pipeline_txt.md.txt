# Session pipeline information files

To be able to process neuroimaging data, processing pipelines (such as HCP minimal processing pipelines) need to be provided with the correct files. When importing data to QuNex—e.g. from raw DICOM files—the information about the session is recorded in the [session acquisition information files](file_session_txt). The information stored, however, often does not enable automatic identification of the image types, e.g., which sequence to use as a high-resolution T1w structural image for atlas registration and brain tissue segmentation. To enable easy and robust identification of sequences, [session acquisition information files](file_session_txt) are converted to "session pipeline information files" by adding descriptors that identify sequence type and (in the case of BOLD and DWI images) provide additional relevant information.

Session information pipeline files are text files. They are expected to be present in each `<study>/sessions/<session id>` folder. By default, they are named `session_<pipeline>.txt` where `<pipeline>` indicates the pipeline for which they are prepared and optimized for. For an overview of the file format, please see information on [session acquisition information files](file_session_txt). This document will present only additional information present in the session pipeline information files.

Session pipeline information files extend [session acquisition information files](file_session_txt) only in the descriptors and information present immediately after the sequence number. The standard descriptors recognized by QuNex are:

* `T1w` — high-res T1 structural image
* `T2w` — high-res T2 structural image
* `FM-Magnitude` — magnitude fieldmap image for structural images
* `FM-Phase` — phase fieldmap image for structural images
* `SE-FM-AP` — spin-echo EPI image for BOLD files acquired in AP direction
* `SE-FM-PA` — spin-echo EPI image for BOLD files acquired in PA direction
* `boldref[n]` — one frame BOLD reference image (replace `[n]` with BOLD index number)
* `bold[n]` — actual BOLD image (replace `[n]` with BOLD index number)
* `DWI` — Diffusion weighted images

In the cases of BOLD and DWI images, another value is expected. For BOLD images it specifies the content of the BOLD (e.g. `rest`, `task`, `encoding`, etc.) and can be used to select only specific images for processing. For DWI images it specifies the variant of the DWI and is used to create a full name for the HCP pipeline. The formula for the DWI specification is: `dir<number of directions acquired>_<phase encoding direction>`, e.g., `dir98_AP`, `dir99_PA`.

## Example

Provided is an example of a [session acquisition information file](file_session_txt) and a related session pipeline information file.

**Session acquisition information file (e.g., `session.txt`)**

``` bash
id: <session_id>
subject: <subject_id>
dicom: /<path_to_sessions_folder>/<session_id>/dicom
raw_data: /<path_to_sessions_folder>/<session_id>/nii
data: /<path_to_sessions_folder>/<session_id>/4dfpimages
hpc: /<path_to_sessions_folder>/<session_id>/hpcpreprocessing

age: 21
handedness: right
gender: male
group: control

institution: MR Imaging Center New Amsterdam
device: Siemens|Prisma_fit|123456

_hcp_brain_size: 150

01: Survey
02: T1w 0.7mm N2 : se(1): DwellTime(0.0000459): UnwarpDir(z)
03: T2w 0.7mm N2 : se(1): DwellTime(0.0000066): UnwarpDir(z)
04: Survey
05: C-BOLD 3mm 48 2.5s FS-P   : se(1): phenc(AP): EchoSpacing(0.0006146)
06: C-BOLD 3mm 48 2.5s FS-A   : se(1): phenc(PA): EchoSpacing(0.0006146)
07: BOLD 3mm 48 2.5s          : se(1): TR(2.5): phenc(PA): EchoSpacing(0.0006029): filename(rest_PA)
08: BOLD 3mm 48 2.5s          : se(1): TR(2.5): phenc(PA): EchoSpacing(0.0006029): filename(task1_PA)
09: BOLD 3mm 48 2.5s          : se(1): TR(2.5): phenc(PA): EchoSpacing(0.0006029): filename(task2_PA)
10: dMRI_dir98_AP             : TR(3.23): PEDirection(j-): EchoSpacing(0.000689998)
11: dMRI_dir98_PA             : TR(3.23): PEDirection(j ): EchoSpacing(0.000689998)
12: dMRI_dir99_AP             : TR(3.23): PEDirection(j-): EchoSpacing(0.000689998)
13: dMRI_dir99_PA             : TR(3.23): PEDirection(j ): EchoSpacing(0.000689998)
```

**A corresponding session pipeline information file (e.g., `session_hcp.txt`)**

``` bash
id: <session_id>
subject: <subject_id>
dicom: /<path_to_sessions_folder>/<session_id>/dicom
raw_data: /<path_to_sessions_folder>/<session_id>/nii
data: /<path_to_sessions_folder>/<session_id>/4dfpimages
hpc: /<path_to_sessions_folder>/<session_id>/hpcpreprocessing

age: 21
handedness: right
gender: male
group: control

institution: MR Imaging Center New Amsterdam
device: Siemens|Prisma_fit|123456

_hcp_brain_size: 150

01:              : Survey
02: T1w          : T1w 0.7mm N2 : se(1): DwellTime(0.0000459): UnwarpDir(z)
03: T2w          : T2w 0.7mm N2 : se(1): DwellTime(0.0000066): UnwarpDir(z)
04:              : Survey
05: SE-FM-AP     : C-BOLD 3mm 48 2.5s FS-P   : se(1): phenc(AP): EchoSpacing(0.0006146)
06: SE-FM-PA     : C-BOLD 3mm 48 2.5s FS-A   : se(1): phenc(PA): EchoSpacing(0.0006146)
07: bold1:rest   : BOLD 3mm 48 2.5s          : se(1): TR(2.5): phenc(PA): EchoSpacing(0.0006029): filename(rest_PA)
08: bold2:task   : BOLD 3mm 48 2.5s          : se(1): TR(2.5): phenc(PA): EchoSpacing(0.0006029): filename(task1_PA)
09: bold2:task   : BOLD 3mm 48 2.5s          : se(1): TR(2.5): phenc(PA): EchoSpacing(0.0006029): filename(task2_PA)
10: DWI:dir98_AP : dMRI_dir98_AP             : TR(3.23): PEDirection(j-): EchoSpacing(0.000689998)
11: DWI:dir98_PA : dMRI_dir98_PA             : TR(3.23): PEDirection(j ): EchoSpacing(0.000689998)
12: DWI:dir99_AP : dMRI_dir99_AP             : TR(3.23): PEDirection(j-): EchoSpacing(0.000689998)
13: DWI:dir99_PA : dMRI_dir99_PA             : TR(3.23): PEDirection(j ): EchoSpacing(0.000689998)
```

Please see [Session acquisition information file](file_session_txt) for information about other elements of the file. See [Preparing the study folder and importing data](../UsageDocs/PreparingStudy.md) for information on how to generate session information files automatically during data import. See the *Mapping data to HCP folder structure* section of the [Running HCP pipeline](../UsageDocs/HCPPreprocessing.md) page for information on how to automatize adding HCP specific descriptors.
