# Mapping specification files

The QuNex mapping file is an essential element of running QuNex and ensuring that the raw `nii` data is onboarded correctly.

The mapping file allows QuNex to map generic `nii` data inputs such that a given pipeline can be readily executed. For instance, the HCP pipelines have an idiosyncratic file I|O naming convention. This makes it hard for users to automate their workflows.

Here QuNex uses a `mapping file` that provides an I|O "bridge" from raw `nii` files to a given folder hierarchy expectation. Here we describe the implementation in the context of HCP minimal processing pipelines:

Specifically, to support correct mapping of `nii` files to the `hcp` folder hierarchy expectation for HCP pipelines, the `session.txt` file has to be expanded, so that each acquired sequence is assigned a unique image descriptor (or "tag").

* If a unique mapping is possible between the original acquisition sequence names and their image descriptors the `create_session_info` command can be used to process the `session.txt` files and automatically generate the resulting `session_<pipeline>.txt` files (default name).

* To use the `create_session_info` command, a mapping specification file has to be provided. By default the `create_session_info` command expects the mapping specification file to be present in: `<study folder>/sessions/specs/<pipeline>_mapping.txt`.

* A comprehensive description of the mapping specification for the Human Connectome Project (HCP) data hierarchy is contained in `~/qunex/python/qx_utilities/templates/hcp_mapping_example.txt`. Upon creation of a new study via `qunex create_study` an example mapping file will be generated in the default location for the user to examine.

## Mapping File Grammar Specification

Mapping file operation involves three files:

* INPUT        --> `session.txt` - session-specific sequence information containing sequence name and number after dcm2nii conversion.

* MAPPING FILE --> `<pipeline>_mapping.txt` - mapping specification file for a given study.

* OUTPUT       --> `session_<pipeline>.txt`- after `create_session_info` on `INPUT` files this adjusted session-specific file will contain `sequence name|number` and `sequence descriptor tag` info for data mapping into a user-specified naming convention.

## Specification for `<pipeline>_mapping.txt` input files syntax

* The file should be a plain text file in which individual mappings are specified one per line:

``` bash
<sequence_name | sequence_number> => <user_defined_sequence_descriptor_tag>
```

## Specification for `session_<pipeline>.txt` output files syntax

* Plain text file with the following:

``` bash
<sequence_number>:<user_defined_sequence_descriptor_tag>:[<user_defined_sequence_info>:]<sequence_name>
```

* For further details on how to specify and use the mapping files please refer to a general overview on [Preparing a study-level mapping file for QuNex workflows pipeline](../UsageDocs/PreparingMappingFile).

* Also see more specific details in the context of [Preparing data for the HCP pipeline](../UsageDocs/PreparingDataHCP) pages.