# Overview of steps for running the HCP pipeline

Prerequisite:

* [Onboarding New Data](OnboardingNewData.md)

As noted, the QuNex Suite implements the HCP pipelines. It introduces and makes use of the modifications that allow processing of both multi-band HCP-compliant data ('HCPStyleData' processing mode) as well as 'legacy' data ('LegacyStyleData' processing mode), which allows preprocessing of imaging data that does not conform to all the imaging standards required by the HCPpipelines. Specifically, it enables preprocessing of 'legacy', single-band BOLD and DWI data, with coarser resolution, longer TRs, and data that is missing field maps and/or a high-resolution T2w image. Of note, using legacy style data will yield inferior results by definition. The user should always attempt to collect data that meets full HCP specifications when starting new data collection.

Preprocessing is implemented in three general steps:

* *[General information on running the HCP pipeline](HCPPreprocessGeneral.md)*
    Information on general settings, folder structure, running commands, logging.

* *[Preparing data for the HCP pipeline](PreparingDataHCP.md)*  
    Specifically, data need to be mapped to a folder structure expected by HCP code along with all  the relevant image parameters needed for preprocessing into a parameter file.

* *[Running the HCP pipeline](RunningPreprocessHCP.md)*  
    Steps for the modified HCP pipelines are described on this page.

* *[Running the HCP ASL pipeline](RunningHCPASL.md)*  
    The HCP ASL pipeline command is described on this page.

* *[Mapping HCP pipeline results to QuNex file hierarchy organization](MappingHCPtoQunex.md)*  
    After completion, the results of the HCP preprocessing are mapped back to the QuNex folder structure for further analyses.

See the linked pages where these three steps and the relevant commands are described in more detail.
