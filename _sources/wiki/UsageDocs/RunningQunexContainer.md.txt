# Running commands against a container using qunex_container


`qunex_container` is an independent, self contained python2.7 and python3 compatible command that enables running commands against either Docker or Singularity QuNex container. The command is available as part of the QuNex suite. To make use of the `qunex_container`, you will have to save a copy of the command to a folder that is in your `PATH` (e.g., `/usr/local/bin` or `/usr/bin`) or add the location of the `qunex_container` to your PATH. You can refer to the [QuNex quick start](https://bitbucket.org/oriadev/qunex/wiki/Overview/QuickStart.md) for details about how to download and install this script.

We suggest adding the following line to your `.bash_profile` or similar shell initialization script:

``` bash
export PATH=<path to folder with qunex_container>:$PATH
```

To be sure that you have the command available, you can inspect the PATH by running `echo $PATH` or the result of `which qunex_container` commands.

Once you have `qunex_container` installed, you can run QuNex commands the same way you would run them inside the container or if QuNex was installed locally, with two differences. First, replace:

``` bash
qunex <command> [<parameters>]
```

with

``` bash
qunex_container <command> [<parameters>]
```

When running `qunex_container` you will need to provide information about the location of the container you want to run the command against. To do this you just set the `container` parameter. An example use in this case would be:

``` bash
qunex_container hcp_freesurfer \
  --sessions="/myStudy/processing/batch.txt" \
  --sessionids="sub1,sub2,sub3" \
  --sessionsfolder="/myStudy/sessions" \
  --container="/mySingularityContainers/qunexcontainer-latest.sif" \
  --overwrite="yes"
```

In the example above some of the parameters are related to the `qunex_container` command (e.g., `container`) while others are related to the `hcp_freesurfer` command (e.g., `sessionids`, `overwrite`, etc.). QuNex automatically distinguishes these parameters and properly propagates them to relevant commands.

The second possibility is to provide the information about the location of the container by setting an environmental variable `QUNEXCONIMAGE`, possibly in your `.bash_profile` or another environment script, e.g.:

* For the Docker container:

``` bash
export QUNEXCONIMAGE="gitlab.qunex.yale.edu:5002/qunex/qunexcontainer:<stable_container_tag>"
```

* For the Singularity container:

``` bash
export QUNEXCONIMAGE="/<path_to_container_image>/<container image name>.sif"
```

If you want to run a set of commands within a container, instead of a single command, you can make use of the `run_turnkey` QuNex commands. Please consult [QuNex quick start](https://bitbucket.org/oriadev/qunex/wiki/Overview/QuickStart.md) and [QuNex 'turnkey' workflow](https://bitbucket.org/oriadev/qunex/wiki/UsageDocs/Turnkey.md) for a detailed description of this.

Another option is to write a script with commands you wish to run within the container and pass the script to the container using the `script` parameter, e.g:

``` bash
qunex_container \
  --script="/myStudy/processing/myScript.sh" \
  --container="/mySingularityContainers/qunexcontainer-latest.sif" 
```

For more detailed information about the use of the `qunex_container` command and how to pass additional settings for the container, consult the inline help by running `qunex_container`.

## Binding/mapping external folders or setting additional container parameters

When using a singularity container you can bind external folders by using the `bind` parameter, this is analog to the singularity's `-B` flag. To bind multiple folders use a comma separated list:

``` bash
qunex_container hcp_freesurfer \
  --sessions="/my_study/processing/batch.txt" \
  --sessionids="sub1,sub2,sub3" \
  --sessionsfolder="/my_study/sessions" \
  --bind="/my_data/data:/data,/my_data/scripts:/scripts"
  --container="/my_singularity_containers/qunexcontainer-latest.sif" \
  --overwrite="yes"
```

When using a docker container, QuNex automatically binds the folder you are currently in as `/data` inside the container. To change this mapping you can use the `dockeropt` parameter. The `dockeropt` is string that lists the additional options to be used when running the Docker container. If the parameter is ommited, the content of `QUNEXDOCKEROPT` environment variable will be used. If neither is specified, the container will mount the current as `data` by specifying the following options `-v "$(pwd)":/data`. The parameters are to be specified in a string exactly as they would be on a command line, e.g. to run in detached mode and mount a specific folder use

``` bash
qunex_container hcp_freesurfer \
  --sessions="/myStudy/processing/batch.txt" \
  --sessionids="sub1,sub2,sub3" \
  --sessionsfolder="/myStudy/sessions" \
  --dockeropt="-d -v /data/host_directory/qx_study/:/my_study/" \
  --container="gitlab.qunex.yale.edu:5002/qunex/qunexcontainer:<tag>" \
  --overwrite="yes"
```
                        
## Using CUDA libraries with a Singularity container

When using CUDA (GPU processing) inside a Singularity container, the `--nv` flag is often required so CUDA drivers and libraries are properly setup. To achieve this, use the `--nv` flag in the `qunex_container` call:

``` bash
qunex_container hcp_diffusion \
  --sessions="/myStudy/processing/batch.txt" \
  --sessionids="sub1" \
  --sessionsfolder="/myStudy/sessions" \
  --nv \
  --container="/mySingularityContainers/qunexcontainer-latest.sif" \
  --overwrite="yes"
```

## Checking QuNex environment status inside the container

You can use the `--env_status` flag to check whether everything is OK with the QuNex environment inside the container. This comes in handy when troubleshooting execution of various commands, the first thing one should check is if the QuNex environment is properly set up.

```bash
qunex_container \
  --env_status \
  --container="/mySingularityContainers/qunexcontainer-latest.sif"
```

## Executing custom bash commands

In the example above we executed the `hcp_diffusion ` command within the container by using the following command:

``` bash
qunex_container hcp_diffusion \
  --sessions="/myStudy/processing/batch.txt" \
  --sessionids="sub1" \
  --sessionsfolder="/myStudy/sessions" \
  --nv \
  --container="/mySingularityContainers/qunexcontainer-latest.sif" \
  --overwrite="yes"
```

Sometimes you would like to execute some additional bash commands, either before entering the container or when already inside the container but before executing the actual QuNex command (`hcp_diffusion ` in the example above). You can do that by using the `bash_pre` and `bash_post` parameters. Commands specified in the `bash_pre` parameter will execute before entering the container while those specified in the `bash_post` parameter will execute when entering the container, but before executing the actual QuNex command. In the example below we first move to a certain directory and load the singularity module (this is done via the `bash_pre` parameter), next we enter the QuNex container, then we set the value of an environmental variable (`FSL_FIXDIR`) via the `bash_post` parameter, finally we execute the `hcp_diffusion ` command.

``` bash
qunex_container hcp_diffusion \
  --sessions="/myStudy/processing/batch.txt" \
  --sessionids="sub1" \
  --sessionsfolder="/myStudy/sessions" \
  --nv \
  --bash_pre="cd /studies/qunex/;module load singularity" \
  --bash_post="export FSL_FIXDIR=/studies/qunex/fix" \
  --container="/mySingularityContainers/qunexcontainer-latest.sif" \
  --overwrite="yes"
```

As you can see multiple bash commands can be chained together by using the semicolon character.

## Scheduling commands to run against the QuNex container on a cluster

To make use of the parallelization in a cluster environment, `qunex_container` can schedule running the specified QuNex command as individual jobs to multiple computer cluster nodes, so that each job will process only one, or a set of sessions (or subjects) in parallel with all the others. Currently PBS, LSF, and SLURM scheduling systems are supported.

Scheduling is enabled by specification of additional parameters to the `qunex_container` command. First, to use a scheduler, provide a scheduler settings string to the `scheduler` parameter. The general information about the settings string is available as inline documentation by running `qunex_container`, whereas more detailed information is available by running `qunex_container ?scheduler`.

An example of scheduling `qunex_container` to a single node:

``` bash
qunex_container hcp_freesurfer \
  --sessions="/myStudy/processing/batch.txt" \
  --sessionids="sub1,sub2,sub3" \
  --sessionsfolder="/myStudy/sessions" \
  --container="/mySingularityContainers/qunexcontainer-latest.sif" \
  --scheduler="SLURM,time=2-00:00:00,ntasks=3,cpus-per-task=2,mem-per-cpu=40000,partition=week" \
  --overwrite="yes" 
```

This command will schedule the QuNex `hcp_freesurfer` command to be run as a single job with three sessions to process.

To distribute commands over multiple jobs and nodes to run in parallel, you have to provide two parameters: 1. `sessions`, and 2. `parsessions`. The first parameter provides a list of sessions (subjects) that need to be processed, the second parameter provides information on how many sessions are to be run in a single job. `qunex_container` will then schedule as many jobs as necessary to process all the sessions, so that at most N (N being equal to `parsessions`) are scheduled per job. An example of scheduling multiple jobs is:

``` bash
qunex_container hcp_freesurfer \
  --sessions="/myStudy/processing/batch.txt" \
  --sessionsfolder="/myStudy/sessions" \
  --container="/mySingularityContainers/qunexcontainer-latest.sif" \
  --parsessions="4" \
  --scheduler="SLURM,time=2-00:00:00,ntasks=4,cpus-per-task=2,mem-per-cpu=40000,partition=week" \
  --overwrite="yes" 
```

If there are 202 sessions listed in the `/myStudy/processing/batch.txt` file, then the upper command will schedule 51 jobs, each job running the `hcp_freesurfer` command against the QuNex container. The first 50 jobs will run 4 sessions in parallel within a container. The last job will run the remaining 2 sessions in parallel within a container.

For more specific information, please see the `qunex_container` inline help by running `qunex_container`.

**NOTE: Passing --sessions parameter values cleanly**  
In the case that --sessions parameter needs to be passed to a command, but not used to split up processing into multiple container calls (e.g., in import_dicom or import_bids commands), specify the sessions to pass to the call using the --csessions parameter. The value of that parameter will be passed on as --sessions parameter to the command.

### Scheduling example

```bash
qunex_container import_dicom \
    --sessionsfolder="/Studies/MyStudy/sessions/" \
    --csessions="ap9374" \
    --masterinbox="none" \
    --check="any" \
    --overwrite="no" \
    --container="/software/Singularity/qunexcontainer-latest.sif" \
    --scheduler="SLURM,time=0-00:20:00,ntasks=1,cpus-per-task=1,mem-per-cpu=20000, \
                 partition=scavenge"
```

In the above case, only one container will be scheduled and within the container the `import_dicom` command will be run with `--sessions="ap9374"`.