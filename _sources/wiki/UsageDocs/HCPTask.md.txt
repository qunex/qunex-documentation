# HCP task fMRI analysis

For all HCP pipeline steps please refer to [Running the HCP pipeline](RunningPreprocessHCP.md). For an overview on how to prepare data and run the HCP preprocessing steps, see [Overview of steps for running the HCP pipeline](HCPPreprocessing.md).

HCP task fMRI analysis is implemented into QuNex via the `hcp_task_fmri_analysis` command. This command runs GLM analyses (either 1st or 2nd level) on HCP preprocessed data.

Besides the shared parameters for HCP functions mentioned in the [General settings and information on HCP preprocessing pipeline Wiki](HCPPreprocessGeneral.md), the following specific parameters are relevant for the `hcp_task_fmri_analysis` command step and should be set either in the command line or in a `batch.txt` file:

``` bash
--hcp_task_lvl1tasks          ... List of task fMRI scan names, which are
                                  the prefixes of the time series filename
                                  for the TaskName task. Multiple task fMRI
                                  scan names should be provided as a comma
                                  separated list. []
--hcp_task_lvl1fsfs           ... List of design names, which are the prefixes
                                  of the fsf filenames for each scan run.
                                  Should contain same number of design files
                                  as time series images in --hcp_task_lvl1tasks
                                  option (N-th design will be used for N-th
                                  time series image). Provide a comma separated
                                  list of design names. If no value is passed
                                  to --hcp_task_lvl1fsfs, the value will be set
                                  to --hcp_task_lvl1tasks.
--hcp_task_lvl2task           ... Name of Level2 subdirectory in which all
                                  Level2 feat directories are written for
                                  TaskName. [NONE]
--hcp_task_lvl2fsf            ... Prefix of design.fsf filename for the Level2
                                  analysis for TaskName. If no value is passed
                                  to --hcp_task_lvl2fsf, the value will be set
                                  to the same list passed to
                                  --hcp_task_lvl2task.
--hcp_task_summaryname        ... Naming convention for single-subject summary
                                  directory. Mandatory when running Level1
                                  analysis only, and should match naming of
                                  Level2 summary directories. Default when running
                                  Level2 analysis is derived from
                                  --hcp_task_lvl2task and --hcp_task_lvl2fsf options
                                  'tfMRI_TaskName/DesignName_TaskName'. [NONE]
--hcp_task_confound           ... Confound matrix text filename (e.g., output
                                  of fsl_motion_outliers). Assumes file is in
                                  <SubjectID>/MNINonLinear/Results/<ScanName>.
                                  [NONE]
--hcp_bold_smoothFWHM         ... Smoothing FWHM that matches what was used in
                                  the fMRISurface pipeline. [2]
--hcp_bold_final_smoothFWHM   ... Value (in mm FWHM) of total desired
                                  smoothing, reached by calculating the
                                  additional smoothing required and applying
                                  that additional amount to data previously
                                  smoothed in fMRISurface. Default=2, which is
                                  no additional smoothing above HCP minimal
                                  preprocessing pipelines outputs.
--hcp_task_highpass           ... Apply additional highpass filter (in seconds)
                                  to time series and task design. This is above
                                  and beyond temporal filter applied during
                                  preprocessing. To apply no additional
                                  filtering, set to 'NONE'. [200]
--hcp_task_lowpass                Apply additional lowpass filter (in seconds)
                                  to time series and task design. This is above
                                  and beyond temporal filter applied during
                                  preprocessing. Low pass filter is generally
                                  not advised for Task fMRI analyses. [NONE]
--hcp_task_procstring         ... String value in filename of time series
                                  image, specifying the additional processing
                                  that was previously applied (e.g.,
                                  FIX-cleaned data with 'hp2000_clean' in
                                  filename). [NONE]
--hcp_regname                 ... Name of surface registration technique.
                                  [MSMSulc]
--hcp_grayordinatesres        ... Value (in mm) that matches value in
                                  'Atlas_ROIs' filename. [2]
--hcp_lowresmesh              ... Value (in mm) that matches surface resolution
                                  for fMRI data. [32]
--hcp_task_vba                ... A flag for using VBA. Only use this flag if you
                                  want unconstrained volumetric blurring of your
                                  data, otherwise set to NO for faster, less
                                  biased, and more senstive processing
                                  (grayordinates results do not use
                                  unconstrained volumetric blurring and are
                                  always produced). This flag is not set by
                                  defult.
--hcp_task_parcellation       ... Name of parcellation scheme to conduct
                                  parcellated analysis. Default setting is
                                  NONE, which will perform dense analysis
                                  instead. Non-greyordinates parcellations
                                  are not supported because they are not valid
                                  for cerebral cortex. Parcellation supersedes
                                  smoothing (i.e. no smoothing is done). [NONE]
--hcp_task_parcellation_file  ... Absolute path to the parcellation dlabel
                                  file [NONE]
```

## Examples

``` bash
# First level HCP TaskfMRIanalysis
qunex hcp_task_fmri_analysis \
    --sessionsfolder="<study_path>/sessions" \
    --sessions="<study_path>/processing/batch.txt" \
    --hcp_task_lvl1tasks="tfMRI_GUESSING_PA" \
    --hcp_task_summaryname="tfMRI_GUESSING/tfMRI_GUESSING"

# Second level HCP TaskfMRIanalysis
qunex hcp_task_fmri_analysis \
    --sessionsfolder="<study_path>/sessions" \
    --sessions="<study_path>/processing/batch.txt" \
    --hcp_task_lvl1tasks="tfMRI_GUESSING_AP@tfMRI_GUESSING_PA" \
    --hcp_task_lvl2task="tfMRI_GUESSING"
```

## hcp_task_fmri_analysis parameter mapping

| QuNex parameter               | HCPpipelines parameter               |
|-------------------------------|--------------------------------------|
|`hcp_task_lvl1task`            |`lvl1tasks`                           |
|`hcp_task_lvl1fsfs`            |`lvl1fsfs`                            |
|`hcp_task_lvl2task`            |`lvl2task`                            |
|`hcp_task_lvl2fsf`             |`lvl2fsf`                             |
|`hcp_task_confound`            |`confound`                            |
|`hcp_bold_smoothFWHM`          |`origsmoothingFWHM`                   |
|`hcp_bold_final_smoothFWHM`    |`finalsmoothingFWHM`                  |
|`hcp_task_highpass`            |`highpassfilter`                      |
|`hcp_task_lowpass`             |`lowpassfilter`                       |
|`hcp_task_procstring`          |`procstring`                          |
|`hcp_regname`                  |`regname`                             |
|`hcp_grayordinatesres`         |`grayordinatesres`                    |
|`hcp_lowresmesh`               |`lowresmesh`                          |
|`hcp_task_vba`                 |`vba`                                 |
|`hcp_task_parcellation`        |`parcellation`                        |
|`hcp_task_parcellation_file`   |`parcellationfile`                    |